Intellij users may run into some trouble importing the project, particularly with the android
portions.

This should give you full IDE support:

  1) Bring up the project structure dialog. (select a project in the project view and press F4)

  2) Select polymorphGlAndroid and set the Module SDK to Android 4.0.3 SDK in the Dependencies
     tab

  3) With polymorphGlAndroid still selected, ensure gl-sources are included as
     dependencies, if not, click the green + and select "Module Dependency" and add it.

This seems to generally work- and provided your polymorphLibrary is the *root* project, intellij
will remember this through restarts.
