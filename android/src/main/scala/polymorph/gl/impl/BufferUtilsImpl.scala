package polymorph.gl.impl

import java.nio._
import polymorph.gl.IBufferUtils
import scala.languageFeature.implicitConversions

class BufferUtilsImpl extends IBufferUtils {
  def allocateByteBuffer(length: Int): ByteBuffer =
    ByteBuffer.allocate(length)

  def allocateByteBuffer(array: Array[Byte]): ByteBuffer =
    ByteBuffer.wrap(array)

  def allocateShortBuffer(length: Int): ShortBuffer =
    ShortBuffer.allocate(length)

  def allocateShortBuffer(array: Array[Short]): ShortBuffer =
    ShortBuffer.wrap(array)

  def allocateIntBuffer(length: Int): IntBuffer =
    IntBuffer.allocate(length)

  def allocateIntBuffer(array: Array[Int]): IntBuffer =
    IntBuffer.wrap(array)

  def allocateFloatBuffer(length: Int): FloatBuffer =
    FloatBuffer.allocate(length)

  def allocateFloatBuffer(array: Array[Float]): FloatBuffer =
    FloatBuffer.wrap(array)

  def size(buffer: Buffer): Int =
    buffer match {
      case x if x == null => 0
      case _: ByteBuffer => 1
      case _: IntBuffer => 4
      case _: ShortBuffer => 2
      case _: FloatBuffer => 4
      case _: DoubleBuffer => 8
      case _: LongBuffer => 8
      case _: CharBuffer => 2
      case _ => throw new RuntimeException("Unexpected buffer type " + buffer.getClass.getName)
    }
}

