package polymorph.gl.impl

import java.nio.{Buffer, FloatBuffer, ByteBuffer, IntBuffer}
import polymorph.gl.{GL => NGL, _}
import polymorph.gfx._
import polymorph.gfx.impl._
import scala.language.implicitConversions
import android.opengl.{GLUtils, GLES20}

private class GLESProgram(val id: Int) extends GLProgram {
  override def toString = s"GLProgram@$id"
}

private class GLESShader(val id: Int) extends GLShader {
  override def toString = s"GLShader@$id"
}

private class GLESBuffer(val id: Int) extends GLBuffer {
  override def toString = s"GLBuffer@$id"
}

private class GLESFramebuffer(val id: Int) extends GLFramebuffer {
  override def toString = s"GLFramebuffer@$id"
}

private class GLESRenderbuffer(val id: Int) extends GLRenderbuffer {
  override def toString = s"GLRenderbuffer@$id"
}

private class GLESTexture(val id: Int) extends GLTexture {
  override def toString = s"GLTexture@$id"
}

private class GLESUniformLocation(val id: Int) extends GLUniformLocation

private [polymorph] class GLES extends NGL {
  private var glShaders = Map[Int, GLShader]()
  private var glBuffers = Map[Int, GLBuffer]()
  private var glTextures = Map[Int, GLTexture]()
  private var glFramebuffers = Map[Int, GLFramebuffer]()
  private var glRenderbuffers = Map[Int, GLRenderbuffer]()
  private var glPrograms = Map[Int, GLProgram]()
  private val paramString = (x: Int) => GLES20.glGetString(x)

  private val paramInt = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    out.get(0)
  }
  private val paramIntN = (n: Int) => {
    (x: Int) => {
      val intBuffer = IntBuffer.allocate(n)
      GLES20.glGetIntegerv(x, intBuffer)
      intBuffer
    }
  }
  private val paramInt2 = paramIntN(2)
  private val paramInt4 = paramIntN(4)
  private val paramUint = (x: Int) => paramInt

  private val paramBool = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetBooleanv(x, out)
    out.get(0) != 0
  }

  private val paramBoolN = (n: Int) => {
    (x: Int) => {
      val byteBuffer = IntBuffer.allocate(n)
      GLES20.glGetBooleanv(x, byteBuffer)
      val outBools = new Array[Boolean](n)
      for(i <- 0 until n)
        outBools(i) = byteBuffer.get(i) != 0
      outBools
    }
  }
  private val paramBool4 = paramBoolN(4)

  private val paramEnum = paramInt
  private val paramFloat = (x: Int) => {
    val out = FloatBuffer.allocate(1)
    GLES20.glGetFloatv(x, out)
    out.get(0)
  }
  private val paramFloatN = (n: Int) => {
    (x: Int) => {
      val floatBuffer = FloatBuffer.allocate(n)
      GLES20.glGetFloatv(x, floatBuffer)
      floatBuffer
    }
  }
  private val paramFloat2 = paramFloatN(2)
  private val paramFloat4 = paramFloatN(4)
  private val paramGLBuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    glBuffers.getOrElse(out.get(0), null)
  }
  private val paramGLFramebuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    glFramebuffers.getOrElse(out.get(0), null)
  }
  private val paramGLProgram = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    glPrograms.getOrElse(out.get(0), null)
  }
  private val paramGLRenderbuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    glRenderbuffers.getOrElse(out.get(0), null)
  }
  private val paramGLTexture = (x: Int) => {
    val out = IntBuffer.allocate(1)
    GLES20.glGetIntegerv(x, out)
    glTextures.getOrElse(out.get(0), null)
  }

  private val parameters = Map[Int, (Int) => Any](
    NGL.ACTIVE_TEXTURE -> paramEnum,
    NGL.ALIASED_LINE_WIDTH_RANGE -> paramFloat2,
    NGL.ALIASED_POINT_SIZE_RANGE -> paramFloat2,
    NGL.ALPHA_BITS -> paramInt,
    NGL.ARRAY_BUFFER_BINDING -> paramGLBuffer,
    NGL.BLEND -> paramBool,
    NGL.BLEND_COLOR -> paramFloat4,
    NGL.BLEND_DST_ALPHA -> paramEnum,
    NGL.BLEND_EQUATION_ALPHA -> paramEnum,
    NGL.BLEND_EQUATION_RGB -> paramEnum,
    NGL.BLEND_SRC_ALPHA -> paramEnum,
    NGL.BLEND_SRC_RGB -> paramEnum,
    NGL.BLUE_BITS -> paramInt,
    NGL.COLOR_CLEAR_VALUE -> paramFloat4,
    NGL.COLOR_WRITEMASK -> paramBool4,
    //    NGL.COMPRESSED_TEXTURE_FORMATS -> ???,
    NGL.CULL_FACE -> paramBool,
    NGL.CULL_FACE_MODE -> paramEnum,
    NGL.CURRENT_PROGRAM -> paramGLProgram,
    NGL.DEPTH_BITS -> paramInt,
    NGL.DEPTH_CLEAR_VALUE -> paramFloat,
    NGL.DEPTH_FUNC -> paramEnum,
    NGL.DEPTH_RANGE -> paramFloat2,
    NGL.DEPTH_TEST -> paramBool,
    NGL.DEPTH_WRITEMASK -> paramBool,
    NGL.DITHER -> paramBool,
    NGL.ELEMENT_ARRAY_BUFFER_BINDING -> paramGLBuffer,
    NGL.FRAMEBUFFER_BINDING -> paramGLFramebuffer,
    NGL.FRONT_FACE -> paramEnum,
    NGL.GENERATE_MIPMAP_HINT -> paramEnum,
    NGL.GREEN_BITS -> paramInt,
    NGL.IMPLEMENTATION_COLOR_READ_FORMAT -> paramEnum,
    NGL.IMPLEMENTATION_COLOR_READ_TYPE -> paramEnum,
    NGL.LINE_WIDTH -> paramFloat,
    NGL.MAX_COMBINED_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_CUBE_MAP_TEXTURE_SIZE -> paramInt,
    NGL.MAX_FRAGMENT_UNIFORM_VECTORS -> paramInt,
    NGL.MAX_RENDERBUFFER_SIZE -> paramInt,
    NGL.MAX_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_TEXTURE_SIZE -> paramInt,
    NGL.MAX_VARYING_VECTORS -> paramInt,
    NGL.MAX_VERTEX_ATTRIBS -> paramInt,
    NGL.MAX_VERTEX_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_VERTEX_UNIFORM_VECTORS -> paramInt,
    NGL.MAX_VIEWPORT_DIMS -> paramInt2,
    NGL.PACK_ALIGNMENT -> paramInt,
    NGL.POLYGON_OFFSET_FACTOR -> paramFloat,
    NGL.POLYGON_OFFSET_FILL -> paramBool,
    NGL.POLYGON_OFFSET_UNITS -> paramFloat,
    NGL.RED_BITS -> paramInt,
    NGL.RENDERBUFFER_BINDING -> paramGLRenderbuffer,
    NGL.RENDERER -> paramString,
    NGL.SAMPLE_BUFFERS -> paramInt,
    NGL.SAMPLE_COVERAGE_INVERT -> paramBool,
    NGL.SAMPLE_COVERAGE_VALUE -> paramFloat,
    NGL.SAMPLES -> paramInt,
    NGL.SCISSOR_BOX -> paramInt4,
    NGL.SCISSOR_TEST -> paramBool,
    NGL.SHADING_LANGUAGE_VERSION -> paramString,
    NGL.STENCIL_BACK_FAIL -> paramEnum,
    NGL.STENCIL_BACK_FUNC -> paramEnum,
    NGL.STENCIL_BACK_PASS_DEPTH_FAIL -> paramEnum,
    NGL.STENCIL_BACK_PASS_DEPTH_PASS -> paramEnum,
    NGL.STENCIL_BACK_REF -> paramEnum,
    NGL.STENCIL_BACK_VALUE_MASK -> paramUint,
    NGL.STENCIL_BACK_WRITEMASK -> paramUint,
    NGL.STENCIL_BITS -> paramInt,
    NGL.STENCIL_CLEAR_VALUE -> paramInt,
    NGL.STENCIL_FAIL -> paramEnum,
    NGL.STENCIL_FUNC -> paramEnum,
    NGL.STENCIL_PASS_DEPTH_FAIL -> paramEnum,
    NGL.STENCIL_PASS_DEPTH_PASS -> paramEnum,
    NGL.STENCIL_REF -> paramEnum,
    NGL.STENCIL_TEST -> paramBool,
    NGL.STENCIL_VALUE_MASK -> paramUint,
    NGL.STENCIL_WRITEMASK -> paramUint,
    NGL.SUBPIXEL_BITS -> paramInt,
    NGL.TEXTURE_BINDING_2D -> paramGLTexture,
    NGL.TEXTURE_BINDING_CUBE_MAP -> paramGLTexture,
    NGL.UNPACK_ALIGNMENT -> paramInt,
    //NGL.UNPACK_COLORSPACE_CONVERSION_WEBGL -> paramEnum,
    //NGL.UNPACK_FLIP_Y_WEBGL -> paramEnum,
    //  NGL.UNPACK_PREMULTIPLY_ALPHA_WEBGL -> paramBoolean,
    NGL.VENDOR -> paramString,
    NGL.VERSION -> paramString,
    NGL.VIEWPORT -> paramInt4
  )

  private implicit def programImp(program: GLProgram): GLESProgram =
    program.asInstanceOf[GLESProgram]
  private implicit def shaderImp(shader: GLShader): GLESShader =
    shader.asInstanceOf[GLESShader]
  private implicit def bufferImp(buffer: GLBuffer): GLESBuffer =
    buffer.asInstanceOf[GLESBuffer]
  private implicit def renderbufferImp(renderbuffer: GLRenderbuffer): GLESRenderbuffer =
    renderbuffer.asInstanceOf[GLESRenderbuffer]
  private implicit def frameBufferImp(framebuffer: GLFramebuffer): GLESFramebuffer =
    framebuffer.asInstanceOf[GLESFramebuffer]
  private implicit def textureImpl(texture: GLTexture): GLESTexture =
    texture.asInstanceOf[GLESTexture]
  private implicit def uniformImpl(uniform: GLUniformLocation): GLESUniformLocation =
    uniform.asInstanceOf[GLESUniformLocation]

  @inline
  def activeTexture(texture: Int): Unit =
    GLES20.glActiveTexture(texture)

  @inline
  def attachShader(program: GLProgram, shader: GLShader): Unit =
    GLES20.glAttachShader(program.id, shader.id)

  @inline
  def bindAttribLocation(program: GLProgram, index: Int, name: String): Unit =
    GLES20.glBindAttribLocation(program.id, index, name)

  @inline
  def bindBuffer(target: Int, buffer: GLBuffer): Unit =
    GLES20.glBindBuffer(target, buffer.id)

  @inline
  def bindFramebuffer(target: Int, framebuffer: GLFramebuffer): Unit =
    GLES20.glBindFramebuffer(target, framebuffer.id)

  @inline
  def bindRenderbuffer(target: Int, renderbuffer: GLRenderbuffer): Unit =
    GLES20.glBindRenderbuffer(target, renderbuffer.id)

  @inline
  def bindTexture(target: Int, texture: GLTexture): Unit =
    GLES20.glBindTexture(target, texture.id)

  @inline
  def blendColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    GLES20.glBlendColor(red, green, blue, alpha)

  @inline
  def blendEquation(mode: Int): Unit =
    GLES20.glBlendEquation(mode)

  @inline
  def blendEquationSeparate(modeRGB: Int, modeAlpha: Int): Unit =
    GLES20.glBlendEquationSeparate(modeRGB, modeAlpha)

  @inline
  def blendFunc(sfactor: Int, dfactor: Int): Unit =
    GLES20.glBlendFunc(sfactor, dfactor)

  @inline
  def blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int): Unit =
    GLES20.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)

  @inline
  def bufferData(target: Int, size: Int, usage: Int): Unit =
    GLES20.glBufferData(target, size, null, usage)

  @inline
  def bufferData(target: Int, data: Buffer, usage: Int): Unit =
    GLES20.glBufferData(target, data.remaining()*BufferUtils.size(data), data, usage)

  @inline
  def bufferSubData(target: Int, offset: Int, data: Buffer): Unit =
    GLES20.glBufferSubData(target, offset, data.remaining()*BufferUtils.size(data), data)

  @inline
  def clear(mask: Int): Unit =
    GLES20.glClear(mask)

  @inline
  def clearColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    GLES20.glClearColor(red, green, blue, alpha)

  @inline
  def clearDepth(depth: Int): Unit =
    GLES20.glClearDepthf(depth)

  @inline
  def clearStencil(s: Int): Unit =
    GLES20.glClearStencil(s)

  @inline
  def colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean): Unit =
    GLES20.glColorMask(red, green, blue, alpha)

  @inline
  def compileShader(shader: GLShader): Unit =
    GLES20.glCompileShader(shader.id)

  @inline
  def compressedTexImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, data: ByteBuffer): Unit =
    GLES20.glCompressedTexImage2D(target, level, internalFormat, width, height, border, data.remaining(), data)

  @inline
  def compressedTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, data: ByteBuffer): Unit =
    GLES20.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, data.remaining(), data)

  @inline
  def copyTexImage2D(target: Int, level: Int, internalFormat: Int, x: Int, y: Int, width: Int, height: Int, border: Int): Unit =
    GLES20.glCopyTexImage2D(target, level, internalFormat, x, y, width, height, border)

  @inline
  def copyTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, x: Int, y: Int, width: Int, height: Int): Unit =
    GLES20.glCopyTexSubImage2D(target, level, xOffset, yOffset, x, y, width, height)

  @inline
  def createBuffer(): GLBuffer = {
    val out = IntBuffer.allocate(1)
    GLES20.glGenBuffers(1, out)
    val buffer = new GLESBuffer(out.get(0))
    glBuffers += buffer.id -> buffer
    buffer
  }

  @inline
  def createFramebuffer(): GLFramebuffer = {
    val out = IntBuffer.allocate(1)
    GLES20.glGenFramebuffers(1, out)
    val framebuffer = new GLESFramebuffer(out.get(0))
    glFramebuffers += framebuffer.id -> framebuffer
    framebuffer
  }

  @inline
  def createProgram(): GLProgram = {
    val program = new GLESProgram(GLES20.glCreateProgram())
    glPrograms += program.id -> program
    program
  }

  @inline
  def createRenderbuffer(): GLRenderbuffer = {
    val out = IntBuffer.allocate(1)
    GLES20.glGenRenderbuffers(1, out)
    val renderbuffer = new GLESRenderbuffer(out.get(0))
    glRenderbuffers += renderbuffer.id -> renderbuffer
    renderbuffer
  }

  @inline
  def createShader(`type`: Int): GLShader = {
    val shader = new GLESShader(GLES20.glCreateShader(`type`))
    glShaders += shader.id -> shader
    shader
  }

  @inline
  def createTexture(): GLTexture = {
    val out = IntBuffer.allocate(1)
    GLES20.glGenTextures(1, out)
    val texture = new GLESTexture(out.get(0))
    glTextures += texture.id -> texture
    texture
  }

  @inline
  def cullFace(mode: Int): Unit =
    GLES20.glCullFace(mode)

  @inline
  def deleteBuffer(buffer: GLBuffer): Unit = {
    glBuffers = glBuffers.filterKeys(_ == buffer.id)
    GLES20.glDeleteBuffers(1, Array(buffer.id), 0)
  }

  @inline
  def deleteFramebuffer(framebuffer: GLFramebuffer): Unit = {
    glFramebuffers = glFramebuffers.filterKeys(_ == framebuffer.id)
    GLES20.glDeleteFramebuffers(1, Array(framebuffer.id), 0)
  }

  @inline
  def deleteProgram(program: GLProgram): Unit = {
    glPrograms = glPrograms.filterKeys(_ == program.id)
    GLES20.glDeleteProgram(program.id)
  }

  @inline
  def deleteRenderbuffer(renderbuffer: GLRenderbuffer): Unit = {
    glRenderbuffers = glRenderbuffers.filterKeys(_ == renderbuffer.id)
    GLES20.glDeleteRenderbuffers(1, Array(renderbuffer.id), 0)
  }

  @inline
  def deleteShader(shader: GLShader): Unit = {
    glShaders = glShaders.filterKeys(_ == shader.id)
    GLES20.glDeleteShader(shader.id)
  }

  @inline
  def deleteTexture(texture: GLTexture): Unit = {
    glTextures = glTextures.filterKeys(_ == texture.id)
    GLES20.glDeleteTextures(1, Array(texture.id), 0)
  }

  @inline
  def depthFunc(func: Int): Unit =
    GLES20.glDepthFunc(func)

  @inline
  def depthMask(flag: Boolean): Unit =
    GLES20.glDepthMask(flag)

  @inline
  def depthRange(zNear: Float, zFar: Float): Unit =
    GLES20.glDepthRangef(zNear, zFar)

  @inline
  def detachShader(program: GLProgram, shader: GLShader): Unit =
    GLES20.glDetachShader(program.id, shader.id)

  @inline
  def disable(cap: Int): Unit =
    GLES20.glDisable(cap)

  @inline
  def disableVertexAttribArray(index: Int): Unit =
    GLES20.glDisableVertexAttribArray(index)

  @inline
  def drawArrays(mode: Int, first: Int, count: Int): Unit =
    GLES20.glDrawArrays(mode, first, count)

  @inline
  def drawElements(mode: Int, count: Int, `type`: Int, offset: Int): Unit =
    GLES20.glDrawElements(mode, count, `type`, offset)

  @inline
  def enable(cap: Int): Unit =
    GLES20.glEnable(cap)

  @inline
  def enableVertexAttribArray(index: Int): Unit =
    GLES20.glEnableVertexAttribArray(index)

  @inline
  def finish(): Unit =
    GLES20.glFinish()

  @inline
  def flush(): Unit =
    GLES20.glFlush()

  @inline
  def framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: GLRenderbuffer): Unit =
    GLES20.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer.id)

  @inline
  def framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: GLTexture, level: Int): Unit =
    GLES20.glFramebufferTexture2D(target, attachment, textarget, texture.id, level)

  @inline
  def frontFace(mode: Int): Unit =
    GLES20.glFrontFace(mode)

  @inline
  def generateMipmap(target: Int): Unit =
    GLES20.glGenerateMipmap(target)

  @inline
  def getActiveAttrib(program: GLProgram, index: Int): GLActiveInfo = {
    val length = new Array[Int](1)
    val size = new Array[Int](1)
    val typ = new Array[Int](1)
    val name = new Array[Byte](512)
    GLES20.glGetActiveAttrib(program.id, index, 512, length, 0, size, 0, typ, 0, name, 0)
    new GLActiveInfo(size(0), typ(0), new String(name, "UTF-8"))
  }

  @inline
  def getActiveUniform(program: GLProgram, index: Int): GLActiveInfo = {
    val length = new Array[Int](1)
    val size = new Array[Int](1)
    val typ = new Array[Int](1)
    val name = new Array[Byte](512)
    GLES20.glGetActiveUniform(program.id, index, 512, length, 0, size, 0, typ, 0, name, 0)
    new GLActiveInfo(size(0), typ(0), new String(name, "UTF-8"))
  }

  @inline
  def getAttachedShaders(program: GLProgram): Seq[GLShader] = {
    val count = IntBuffer.allocate(1)
    val shaders = IntBuffer.allocate(256)
    GLES20.glGetAttachedShaders(program.id, 256, count, shaders)
    (for(i <- 0 until count.get(0)) yield glShaders(shaders.get(i))).toSeq
  }

  @inline
  def getAttribLocation(program: GLProgram, name: String): Int =
    GLES20.glGetAttribLocation(program.id, name)

  @inline
  def getBufferParameter(target: Int, pname: Int): Int = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetBufferParameteriv(target, pname, out)
    out.get(0)
  }

  @inline
  def getParameter(param: Int): Any =
    parameters(param)(param)

  @inline
  def getError(): Int =
    GLES20.glGetError()

  @inline
  def getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any = {
    val output = IntBuffer.allocate(0)
    GLES20.glGetFramebufferAttachmentParameteriv(target, attachment, pname, output)
    if(pname == NGL.FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE)
      glRenderbuffers.get(output.get(0))
    else
      output.get(0)
  }

  @inline
  def getProgramParameter(program: GLProgram, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetProgramiv(program.id, pname, out)
    if(pname == NGL.DELETE_STATUS || pname == NGL.LINK_STATUS || pname == NGL.VALIDATE_STATUS)
      out.get(0) != 0
    else
      out.get(0)
  }

  @inline
  def getProgramInfoLog(program: GLProgram): String =
    GLES20.glGetProgramInfoLog(program.id)

  @inline
  def getRenderbufferParameter(target: Int, pname: Int): Int = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetRenderbufferParameteriv(target, pname, out)
    out.get(0)
  }

  @inline
  def getShaderParameter(shader: GLShader, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetShaderiv(shader.id, pname, out)
    if (pname == NGL.SHADER_TYPE)
      out.get(0)
    else
      out.get(0) != 0
  }

  @inline
  def getShaderPrecisionFormat(shaderType: Int, precisionType: Int): GLShaderPrecisionFormat = {
    val range = IntBuffer.allocate(2)
    val precision = IntBuffer.allocate(1)
    GLES20.glGetShaderPrecisionFormat(shaderType, precisionType, range, precision)
    new GLShaderPrecisionFormat(range.get(0), range.get(1), precision.get(0))
  }

  @inline
  def getShaderInfoLog(shader: GLShader): String =
    GLES20.glGetShaderInfoLog(shader.id)

  @inline
  def getShaderSource(shader: GLShader): String = {
    val length = Array[Int](1)
    val source = new Array[Byte](1024*1024)
    GLES20.glGetShaderSource(shader.id, 1024*1024, length, 0, source, 0)
    new String(source, "UTF-8")
  }

  @inline
  def getTexParameter(target: Int, pname: Int): Int = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetTexParameteriv(target, pname, out)
    out.get(0)
  }

  //  def getUniform(program: GLProgram, location: GLUniformLocation): Any

  @inline
  def getUniformLocation(program: GLProgram, name: String): GLUniformLocation =
    new GLESUniformLocation(GLES20.glGetUniformLocation(program.id, name))

  def getVertexAttrib(index: Int, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    GLES20.glGetVertexAttribiv(index, pname, out)
    pname match {
      case NGL.VERTEX_ATTRIB_ARRAY_BUFFER_BINDING =>
        glBuffers.getOrElse(out.get(0), null)
      case NGL.VERTEX_ATTRIB_ARRAY_ENABLED =>
        out.get(0) != 0
      case NGL.VERTEX_ATTRIB_ARRAY_SIZE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_STRIDE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_TYPE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_NORMALIZED =>
        out.get(0) != 0
      case NGL.CURRENT_VERTEX_ATTRIB =>
        out
    }
  }

  @inline
  def hint(target: Int, mode: Int): Unit =
    GLES20.glHint(target, mode)

  @inline
  def isBuffer(buffer: GLBuffer): Boolean =
    buffer != null && GLES20.glIsBuffer(buffer.id)

  @inline
  def isEnabled(cap: Int): Boolean =
    GLES20.glIsEnabled(cap)

  @inline
  def isFramebuffer(framebuffer: GLFramebuffer): Boolean =
    framebuffer != null && GLES20.glIsFramebuffer(framebuffer.id)

  @inline
  def isProgram(program: GLProgram): Boolean =
    program != null && GLES20.glIsProgram(program.id)

  @inline
  def isRenderbuffer(renderbuffer: GLRenderbuffer): Boolean =
    renderbuffer != null && GLES20.glIsRenderbuffer(renderbuffer.id)

  @inline
  def isShader(shader: GLShader): Boolean =
    shader != null && GLES20.glIsShader(shader.id)

  @inline
  def isTexture(texture: GLTexture): Boolean =
    texture != null && GLES20.glIsTexture(texture.id)

  @inline
  def lineWidth(width: Float): Unit =
    GLES20.glLineWidth(width)

  @inline
  def linkProgram(program: GLProgram): Unit =
    GLES20.glLinkProgram(program.id)

  @inline
  def pixelStorei(pname: Int, param: Int): Unit =
    GLES20.glPixelStorei(pname, param)

  @inline
  def polygonOffset(factor: Float, units: Float): Unit =
    GLES20.glPolygonOffset(factor, units)

  @inline
  def readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    GLES20.glReadPixels(x, y, width, height, format, `type`, pixels)

  @inline
  def renderbufferStorage(target: Int, internalFormat: Int, width: Int, height: Int): Unit =
    GLES20.glRenderbufferStorage(target, internalFormat, width, height)

  @inline
  def sampleCoverage(value: Int, invert: Boolean): Unit =
    GLES20.glSampleCoverage(value, invert)

  @inline
  def scissor(x: Int, y: Int, width: Int, height: Int): Unit =
    GLES20.glScissor(x, y, width, height)

  @inline
  def shaderSource(shader: GLShader, source: String): Unit =
    GLES20.glShaderSource(shader.id, source)

  @inline
  def stencilFunc(func: Int, ref: Int, mask: Int): Unit =
    GLES20.glStencilFunc(func, ref, mask)

  @inline
  def stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int): Unit =
    GLES20.glStencilFuncSeparate(face, func, ref, mask)

  @inline
  def stencilMask(mask: Int): Unit =
    GLES20.glStencilMask(mask)

  @inline
  def stencilMaskSeparate(face: Int, mask: Int): Unit =
    GLES20.glStencilMaskSeparate(face, mask)

  @inline
  def stencilOp(fail: Int, zFail: Int, zPass: Int): Unit =
    GLES20.glStencilOp(fail, zFail, zPass)

  @inline
  def stencilOpSeparate(face: Int, fail: Int, zFail: Int, zPass: Int): Unit =
    GLES20.glStencilOpSeparate(face, fail, zFail, zPass)

  @inline
  def texImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    GLES20.glTexImage2D(target, level, internalFormat, width, height, border, format, `type`, pixels)

  def texImage2D(target: Int, level: Int, internalFormat: Int, format: Int, `type`: Int, pixels: Image): Unit =
    GLUtils.texImage2D(target, level, internalFormat, pixels.asInstanceOf[ImageImpl].peer, `type`, 0)

  @inline
  def texParameterf(target: Int, pname: Int, param: Float): Unit =
    GLES20.glTexParameterf(target, pname, param)

  @inline
  def texParameteri(target: Int, pname: Int, param: Int): Unit =
    GLES20.glTexParameteri(target, pname, param)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    GLES20.glTexSubImage2D(target, level, xOffset, yOffset, width, height, format, `type`, pixels)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, format: Int, `type`: Int, pixels: Image): Unit =
    GLUtils.texSubImage2D(target, level, xOffset, yOffset, pixels.asInstanceOf[ImageImpl].peer, format, `type`)

  @inline
  def uniform1f(location: GLUniformLocation, x: Float): Unit =
    GLES20.glUniform1f(location.id, x)

  @inline
  def uniform1fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    GLES20.glUniform1fv(location.id, 1, v)

  @inline
  def uniform1fv(location: GLUniformLocation, v: Array[Float]): Unit =
    GLES20.glUniform1fv(location.id, 1, v, 0)

  @inline
  def uniform1i(location: GLUniformLocation, x: Int): Unit =
    GLES20.glUniform1i(location.id, x)

  @inline
  def uniform1iv(location: GLUniformLocation, v: IntBuffer): Unit =
    GLES20.glUniform1iv(location.id, 1, v)

  @inline
  def uniform1iv(location: GLUniformLocation, v: Array[Int]): Unit =
    GLES20.glUniform1iv(location.id, 1, v, 0)

  @inline
  def uniform2f(location: GLUniformLocation, x: Float, y: Float): Unit =
    GLES20.glUniform2f(location.id, x, y)

  @inline
  def uniform2fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    GLES20.glUniform2fv(location.id, 1, v)

  @inline
  def uniform2fv(location: GLUniformLocation, v: Array[Float]): Unit =
    GLES20.glUniform2fv(location.id, 1, v, 0)

  @inline
  def uniform2i(location: GLUniformLocation, x: Int, y: Int): Unit =
    GLES20.glUniform2i(location.id, x, y)

  @inline
  def uniform2iv(location: GLUniformLocation, v: IntBuffer): Unit =
    GLES20.glUniform2iv(location.id, 1, v)

  @inline
  def uniform2iv(location: GLUniformLocation, v: Array[Int]): Unit =
    GLES20.glUniform2iv(location.id, 1, v, 0)

  @inline
  def uniform3f(location: GLUniformLocation, x: Float, y: Float, z: Float): Unit =
    GLES20.glUniform3f(location.id, x, y, z)

  @inline
  def uniform3fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    GLES20.glUniform3fv(location.id, 1, v)

  @inline
  def uniform3fv(location: GLUniformLocation, v: Array[Float]): Unit =
    GLES20.glUniform3fv(location.id, 1, v, 0)

  @inline
  def uniform3i(location: GLUniformLocation, x: Int, y: Int, z: Int): Unit =
    GLES20.glUniform3i(location.id, x, y, z)

  @inline
  def uniform3iv(location: GLUniformLocation, v: IntBuffer): Unit =
    GLES20.glUniform3iv(location.id, 1, v)

  @inline
  def uniform3iv(location: GLUniformLocation, v: Array[Int]): Unit =
    GLES20.glUniform3iv(location.id, 1, v, 0)

  @inline
  def uniform4f(location: GLUniformLocation, x: Float, y: Float, z: Float, w: Float): Unit =
    GLES20.glUniform4f(location.id, x, y, z, w)

  @inline
  def uniform4fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    GLES20.glUniform4fv(location.id, 1, v)

  @inline
  def uniform4fv(location: GLUniformLocation, v: Array[Float]): Unit =
    GLES20.glUniform4fv(location.id, 1, v, 0)

  @inline
  def uniform4i(location: GLUniformLocation, x: Int, y: Int, z: Int, w: Int): Unit =
    GLES20.glUniform4i(location.id, x, y, z, w)

  @inline
  def uniform4iv(location: GLUniformLocation, v: IntBuffer): Unit =
    GLES20.glUniform4iv(location.id, 1, v)

  @inline
  def uniform4iv(location: GLUniformLocation, v: Array[Int]): Unit =
    GLES20.glUniform4iv(location.id, 1, v, 0)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    GLES20.glUniformMatrix2fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    GLES20.glUniformMatrix2fv(location.id, 1, transpose, value, 0)

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    GLES20.glUniformMatrix3fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    GLES20.glUniformMatrix3fv(location.id, 1, transpose, value, 0)

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    GLES20.glUniformMatrix4fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    GLES20.glUniformMatrix4fv(location.id, 1, transpose, value, 0)

  @inline
  def useProgram(program: GLProgram): Unit =
    GLES20.glUseProgram(program.id)

  @inline
  def validateProgram(program: GLProgram): Unit =
    GLES20.glValidateProgram(program.id)

  @inline
  def vertexAttrib1f(indx: Int, x: Float): Unit =
    GLES20.glVertexAttrib1f(indx, x)

  @inline
  def vertexAttrib1fv(indx: Int, values: FloatBuffer): Unit =
    GLES20.glVertexAttrib1f(indx, values.get(0))

  @inline
  def vertexAttrib1fv(indx: Int, values: Array[Float]): Unit =
    GLES20.glVertexAttrib1f(indx, values(0))

  @inline
  def vertexAttrib2f(indx: Int, x: Float, y: Float): Unit =
    GLES20.glVertexAttrib2f(indx, x, y)

  @inline
  def vertexAttrib2fv(indx: Int, values: FloatBuffer): Unit =
    GLES20.glVertexAttrib2f(indx, values.get(0), values.get(1))

  @inline
  def vertexAttrib2fv(indx: Int, values: Array[Float]): Unit =
    GLES20.glVertexAttrib2f(indx, values(0), values(1))

  @inline
  def vertexAttrib3f(indx: Int, x: Float, y: Float, z: Float): Unit =
    GLES20.glVertexAttrib3f(indx, x, y, z)

  @inline
  def vertexAttrib3fv(indx: Int, values: FloatBuffer): Unit =
    GLES20.glVertexAttrib3f(indx, values.get(0), values.get(1), values.get(2))

  @inline
  def vertexAttrib3fv(indx: Int, values: Array[Float]): Unit =
    GLES20.glVertexAttrib3f(indx, values(0), values(1), values(2))

  @inline
  def vertexAttrib4f(indx: Int, x: Float, y: Float, z: Float, w: Float): Unit =
    GLES20.glVertexAttrib4f(indx, x, y, z, w)

  @inline
  def vertexAttrib4fv(indx: Int, values: FloatBuffer): Unit =
    GLES20.glVertexAttrib4f(indx, values.get(0), values.get(1), values.get(2), values.get(3))

  @inline
  def vertexAttrib4fv(indx: Int, values: Array[Float]): Unit =
    GLES20.glVertexAttrib4f(indx, values(0), values(1), values(2), values(3))

  @inline
  def vertexAttribPointer(indx: Int, size: Int, `type`: Int, normalized: Boolean, stride: Int, offset: Int): Unit =
    GLES20.glVertexAttribPointer(indx, size, `type`, normalized, stride, offset)

  @inline
  def viewport(x: Int, y: Int, width: Int, height: Int): Unit =
    GLES20.glViewport(x, y, width, height)
}
