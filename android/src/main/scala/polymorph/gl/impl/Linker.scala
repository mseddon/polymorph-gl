package polymorph.gl.impl
import polymorph.gl._
import polymorph.ui._
import polymorph.ui.impl._

/**
 * Created by Matt on 31/07/2015.
 */
object Linker {
  def link(): Unit = {
    RootGLView._peer = RootGLViewImpl.apply
    BufferUtils.impl = new BufferUtilsImpl
    polymorph.Platform._supportsOpenGL = true
  }
}
