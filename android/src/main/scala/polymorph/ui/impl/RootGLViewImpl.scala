package polymorph.ui.impl

import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

import android.content.Context
import android.graphics.{Canvas, Paint}
import android.opengl.GLSurfaceView
import android.opengl.GLSurfaceView.Renderer
import android.view.View
import android.view.inputmethod.InputMethodManager
import polymorph.event.impl.{KeyEventReceiverProxy, MouseEventReceiverProxy, TouchEventReceiverProxy}
import polymorph.gfx.impl.AndroidGfx
import polymorph.gl.GL
import polymorph.gl.impl.GLES
import polymorph.impl.AppImpl
import polymorph.ui.{RootGfxViewPeer, RootGfxView, RootGLViewPeer, RootGLView}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 31/07/2015.
 */
class RootGLViewImpl(view: RootGLView, _title: String, withId: String) extends RootGLViewPeer(view)  {

  private val gl: GL = new GLES()


  val _peer: GLSurfaceView = new GLSurfaceView(AppImpl.activity)
      with TouchEventReceiverProxy
      with MouseEventReceiverProxy
      with KeyEventReceiverProxy {
    val receiver = view
    val paint = new Paint()
    setEGLContextClientVersion(2)
    setEGLConfigChooser(8, 8, 8, 0, 16, 8)
    setRenderer(new Renderer {
      override def onSurfaceChanged(gl: GL10, width: Int, height: Int): Unit = {
        RootGLViewImpl.this.gl.viewport(0, 0, width, height)
      }

      override def onSurfaceCreated(gl: GL10, config: EGLConfig): Unit =
        view.init(RootGLViewImpl.this.gl)

      override def onDrawFrame(gl: GL10): Unit = {
        RootGLViewImpl.this.gl.viewport(0, 0, width.toInt, height.toInt)
        view.paint(RootGLViewImpl.this.gl)
        RootGLViewImpl.this.gl.finish()
      }
    })
  }

  _peer.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY)

  AppImpl.activity.setContentView(_peer)

  override def globalPosition: Vec2 = Vec2()

  override def visible_=(visible: Boolean): Unit = _peer.setVisibility(if(visible) View.VISIBLE else View.GONE)

  override def repaint(): Unit = _peer.postInvalidate()

  override def visible: Boolean = _peer.getVisibility == View.VISIBLE

  override def title: String = ""

  override def height: Float = _peer.getHeight

  override def width: Float = _peer.getWidth

  override def title_=(title: String): Unit = {}

  def focus(): Unit = {
    _peer.requestFocus()
    val imm = AppImpl.activity.getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
  }

  def blur(): Unit = {
    _peer.clearFocus()
    val imm = AppImpl.activity.getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
    imm.hideSoftInputFromInputMethod(_peer.getWindowToken, 0)
  }


  /**
   * All OpenGL calls *must* occur on the opengl thread, otherwise they will fail silently.
   * This allow this behaviour within arbitrary futures and functions.
   *
   * @param body the body to invoke, a 1-ary function which is passed the GL context.
   *             only withGL blocks, init, display and dispose are guaranteed to have access to a valid
   *             opengl context.  In particular- <i>it is possible for opengl calls outside of these scopes to
   *             fail silently, without showing up in gl.getError</i>
   */
  def withGL(body: GL => Unit): Unit = {
    val runnable = new Runnable {
      override def run(): Unit = {
        body(gl)
        synchronized {
          notify()
        }
      }
    }
    _peer.queueEvent(runnable)
    try {
      runnable.wait()
    } catch {
      case _: Throwable =>
    }
  }

  var _animate = false
  def animate = _animate
  def animate_=(b: Boolean): Unit = {
    _animate = b
    if(b)
      _peer.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY)
    else
      _peer.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY)
  }
}


object RootGLViewImpl {
  def apply(view: RootGLView, title: String, withId: String): RootGLViewPeer =
    new RootGLViewImpl(view, title, withId)
}
