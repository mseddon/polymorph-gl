package polymorph.gl.impl;

import java.nio.*;

import org.robovm.rt.bro.*;
import org.robovm.rt.bro.annotation.*;
import org.robovm.rt.bro.ptr.BytePtr;

class ArrayOfStrings extends Struct<ArrayOfStrings> {
    @StructMember(0)
    public native @ByRef BytePtr string();

    @StructMember(0)
    public native @ByRef void string(BytePtr value);
}

@Library("OpenGLES")
public class GLES2 {
    static {
        Bro.bind(GLES2.class);
    }

    public static final int GL_INFO_LOG_LENGTH = 35716;
    public static final int GL_SHADER_SOURCE_LENGTH = 35720;


    @Bridge
    public static native void glActiveTexture(int texture);
        
    @Bridge
    public static native void glAttachShader(int program, int shader);

    @Bridge
    public static native void glBindAttribLocation(int program, int index, String name);

    @Bridge    
    public static native void glBindBuffer(int target, int buffer);

    @Bridge
    public static native void glBindFramebuffer(int target, int framebuffer);

    @Bridge
    public static native void glBindRenderbuffer(int target, int framebuffer);

    @Bridge
    public static native void glBindTexture(int target, int texture);

    @Bridge
    public static native void glBlendColor(float red, float green, float blue, float alpha);

    @Bridge
    public static native void glBlendEquation(int mode);

    @Bridge
    public static native void glBlendEquationSeparate(int modeRGB, int modeAlpha);

    @Bridge
    public static native void glBlendFunc(int sfactor, int dfactor);

    @Bridge
    public static native void glBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);

    @Bridge
    public static native void glBufferData(int target, int size, Buffer data, int usage);

    @Bridge
    public static native void glBufferSubData(int target, int offset, int size, Buffer data);

    @Bridge
    public static native void glClear(int mask);

    @Bridge
    public static native void glClearColor(float red, float green, float blue, float alpha);

    @Bridge
    public static native void glClearDepthf(float depth);

    @Bridge
    public static native void glClearStencil(int s);

    @Bridge
    public static native void glColorMask(boolean red, boolean green, boolean blue, boolean alpha);

    @Bridge
    public static native void glCompileShader(int shader);

    @Bridge
    public static native void glCompressedTexImage2D(int target, int level, int internalFormat, int width, int height, int border, ByteBuffer data);

    @Bridge
    public static native void glCompressedTexSubImage2D(int target, int level, int xOffset, int yOffset, int width, int height, int format, ByteBuffer data);

    @Bridge
    public static native void glCopyTexImage2D(int target, int level, int internalFormat, int x, int y, int width, int height, int border);

    @Bridge
    public static native void glCopyTexSubImage2D(int target, int level, int xOffset, int yOffset, int x, int y, int width, int height);

    @Bridge
    public static native int glCreateProgram();

    @Bridge
    public static native int glCreateShader(int type);

    @Bridge
    public static native void glCullFace(int mode);

    @Bridge
    public static native void glDeleteBuffers(int size, int buffers[]);

    @Bridge
    public static native void glDeleteFramebuffers(int size, int[] framebuffer);

    @Bridge
    public static native void glDeleteProgram(int program);

    @Bridge
    public static native void glDeleteRenderbuffers(int size, int[] renderbuffers);

    @Bridge
    public static native void glDeleteShader(int shader);

    @Bridge
    public static native void glDeleteTextures(int size, int[] textures);

    @Bridge
    public static native void glDepthFunc(int func);

    @Bridge
    public static native void glDepthMask(boolean flag);

    @Bridge
    public static native void glDepthRangef(float zNear, float zFar);

    @Bridge
    public static native void glDetachShader(int program, int shader);

    @Bridge
    public static native void glDisable(int cap);

    @Bridge
    public static native void glDisableVertexAttribArray(int index);

    @Bridge
    public static native void glDrawArrays(int mode, int first, int count);

    @Bridge
    public static native void glDrawElements(int mode, int count, int type, int offset);

    @Bridge
    public static native void glEnable(int cap);

    @Bridge
    public static native void glEnableVertexAttribArray(int index);

    @Bridge
    public static native void glFinish();

    @Bridge
    public static native void glFlush();

    @Bridge
    public static native void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer);

    @Bridge
    public static native void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level);

    @Bridge
    public static native void glFrontFace(int mode);

    @Bridge
    public static native int glGenBuffers(int size, int[] buffers);

    @Bridge
    public static native int glGenBuffers(int size, IntBuffer buffers);

    @Bridge
    public static native int glGenFramebuffers(int size, int[] framebuffers);

    @Bridge
    public static native int glGenFramebuffers(int size, IntBuffer framebuffers);

    @Bridge
    public static native int glGenRenderbuffers(int size, int[] renderbuffers);

    @Bridge
    public static native int glGenRenderbuffers(int size, IntBuffer renderbuffers);

    @Bridge
    public static native int glGenTextures(int size, int[] textures);

    @Bridge
    public static native int glGenTextures(int size, IntBuffer textures);

    @Bridge
    public static native void glGenerateMipmap(int target);

    @Bridge
    public static native void glGetBooleanv(int pname, int[] params);
    @Bridge
    public static native void glGetBooleanv(int pname, IntBuffer params);

    @Bridge
    public static native void glGetFloatv(int pnam, float[] params);
    @Bridge
    public static native void glGetFloatv(int pnam, FloatBuffer params);

    @Bridge
    public static native void glGetIntegerv(int pname, int[] params);

    @Bridge
    public static native void glGetIntegerv(int pname, IntBuffer params);

    @Bridge
    public static native void glGetActiveAttrib(int program, int index, int bufSize, int[] length, int[] size, int[] type, byte[] name);

    @Bridge
    public static native void glGetActiveAttrib(int program, int index, int bufSize, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name);

    @Bridge
    public static native void glGetActiveUniform(int program, int index, int bufSize, int[] length, int[] size, int[] type, byte[] name);

    @Bridge
    public static native void glGetAttachedShaders(int program, int maxCount, int[] count, int[] shaders);

    @Bridge
    public static native void glGetAttachedShaders(int program, int maxCount, IntBuffer count, IntBuffer shaders);

    @Bridge
    public static native int glGetAttribLocation(int program, String name);

    @Bridge
    public static native void glGetBufferParameteriv(int target, int value, int[] data);

    @Bridge
    public static native void glGetBufferParameteriv(int target, int value, IntBuffer data);

    @Bridge
    public static native int glGetError();

    @Bridge
    public static native void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, int[] params);

    @Bridge
    public static native void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params);

    @Bridge
    public static native void glGetProgramInfoLog(int program, int maxLength, int[] length, byte[] infoLog);

    @Bridge
    public static native void glGetProgramInfoLog(int program, int maxLength, IntBuffer length, ByteBuffer infoLog);

    @Bridge
    public static native void glGetProgramiv(int program, int pname, int[] params);

    @Bridge
    public static native void glGetProgramiv(int program, int pname, IntBuffer params);

    @Bridge
    public static native void glGetRenderbufferParameteriv(int target, int pname, int[] params);

    @Bridge
    public static native void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params);

    @Bridge
    public static native void glGetShaderInfoLog(int shader, int maxLength, int[] length, byte[] infoLog);

    @Bridge
    public static native void glGetShaderPrecisionFormat(int shaderType, int precisionType, int[] range, int[] precision);

    @Bridge
    public static native void glGetShaderPrecisionFormat(int shaderType, int precisionType, IntBuffer range, IntBuffer precision);

    @Bridge
    public static native void glGetShaderSource(int shader, int bufSize, int[] length, byte[] source);

    @Bridge
    public static native void glGetShaderSource(int shader, int bufSize, IntBuffer length, ByteBuffer source);

    @Bridge
    public static native void glGetShaderiv(int shader, int pname, int[] params);

    @Bridge
    public static native void glGetShaderiv(int shader, int pname, IntBuffer params);

    @Bridge
    public static native void glGetString(int name);

    @Bridge
    public static native void glGetTexParameterfv(int target, int pname, float[] params);

    @Bridge
    public static native void glGetTexParameterfv(int target, int pname, FloatBuffer params);

    @Bridge
    public static native void glGetTexParameteriv(int target, int pname, int[] params);

    @Bridge
    public static native void glGetTexParameteriv(int target, int pname, IntBuffer params);

    @Bridge
    public static native void glGetUniformfv(int program, int location, float[] params);

    @Bridge
    public static native void glGetUniformfv(int program, int location, FloatBuffer params);

    @Bridge
    public static native void glGetUniformiv(int program, int location, int[] params);

    @Bridge
    public static native void glGetUniformiv(int program, int location, IntBuffer params);

    @Bridge
    public static native int glGetUniformLocation(int program, String name);

    @Bridge
    public static native void glGetVertexAttribfv(int index, int pname, float[] params);

    @Bridge
    public static native void glGetVertexAttribfv(int index, int pname, FloatBuffer params);

    @Bridge
    public static native void glGetVertexAttribiv(int index, int pname, int[] params);

    @Bridge
    public static native void glGetVertexAttribiv(int index, int pname, IntBuffer params);

    // glGetVertexAttribPointerv

    @Bridge
    public static native void glHint(int target, int mode);

    @Bridge
    public static native boolean glIsBuffer(int buffer);

    @Bridge
    public static native boolean glIsEnabled(int cap);

    @Bridge
    public static native boolean glIsFramebuffer(int framebuffer);

    @Bridge
    public static native boolean glIsProgram(int program);

    @Bridge
    public static native boolean glIsRenderbuffer(int renderbuffer);

    @Bridge
    public static native boolean glIsShader(int shader);

    @Bridge
    public static native boolean glIsTexture(int stexture);

    @Bridge
    public static native void glLineWidth(float width);

    @Bridge
    public static native void glLinkProgram(int program);

    @Bridge
    public static native void glPixelStorei(int pname, int param);

    @Bridge
    public static native void glPolygonOffset(float factor, float units);

    @Bridge
    public static native void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer data);

    @Bridge
    public static native void glReleaseShaderCompiler();

    @Bridge
    public static native void glRenderbufferStorage(int target, int internalformat, int width, int height);

    @Bridge
    public static native void glSampleCoverage(float value, boolean invert);

    @Bridge
    public static native void glScissor(int x, int y, int width, int height);

    @Bridge
    public static native void glShaderBinary(int size, int[] shaders, int binaryFormat, Buffer binary, int[] length);

    @Bridge
    public static native void glShaderSource(int shader, int count, ArrayOfStrings strings, int[] length);

    @Bridge
    public static native void glStencilFunc(int func, int ref, int mask);

    @Bridge
    public static native void glStencilFuncSeparate(int face, int func, int ref, int mask);

    @Bridge
    public static native void glStencilMask(int mask);

    @Bridge
    public static native void glStencilMaskSeparate(int face, int mask);

    @Bridge
    public static native void glStencilOp(int sfail, int dpfail, int dppass);

    @Bridge
    public static native void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass);

    @Bridge
    public static native void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer data);

    @Bridge
    public static native void glTexParameterf(int target, int pname, float param);

    @Bridge
    public static native void glTexParameteri(int target, int pname, int param);

    @Bridge
    public static native void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer data);

    @Bridge
    public static native void glUniform1f(int location, float v0);

    @Bridge
    public static native void glUniform2f(int location, float v0, float v1);

    @Bridge
    public static native void glUniform3f(int location, float v0, float v1, float v2);

    @Bridge
    public static native void glUniform4f(int location, float v0, float v1, float v3, float v4);

    @Bridge
    public static native void glUniform1i(int location, int v0);

    @Bridge
    public static native void glUniform2i(int location, int v0, int v1);

    @Bridge
    public static native void glUniform3i(int location, int v0, int v1, int v2);

    @Bridge
    public static native void glUniform4i(int location, int v0, int v1, int v3, int v4);

    @Bridge
    public static native void glUniform1fv(int location, int count, float[] value);

    @Bridge
    public static native void glUniform1fv(int location, int count, FloatBuffer value);

    @Bridge
    public static native void glUniform2fv(int location, int count, float[] value);

    @Bridge
    public static native void glUniform2fv(int location, int count, FloatBuffer value);

    @Bridge
    public static native void glUniform3fv(int location, int count, float[] value);

    @Bridge
    public static native void glUniform3fv(int location, int count, FloatBuffer value);

    @Bridge
    public static native void glUniform4fv(int location, int count, float[] value);

    @Bridge
    public static native void glUniform4fv(int location, int count, FloatBuffer value);

    @Bridge
    public static native void glUniform1iv(int location, int count, int[] value);

    @Bridge
    public static native void glUniform1iv(int location, int count, IntBuffer value);

    @Bridge
    public static native void glUniform2iv(int location, int count, int[] value);

    @Bridge
    public static native void glUniform2iv(int location, int count, IntBuffer value);

    @Bridge
    public static native void glUniform3iv(int location, int count, int[] value);

    @Bridge
    public static native void glUniform3iv(int location, int count, IntBuffer value);

    @Bridge
    public static native void glUniform4iv(int location, int count, int[] value);

    @Bridge
    public static native void glUniform4iv(int location, int count, IntBuffer value);

    @Bridge
    public static native void glUniformMatrix2fv(int location, int count, boolean transpose, float[] value);

    @Bridge
    public static native void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value);

    @Bridge
    public static native void glUniformMatrix3fv(int location, int count, boolean transpose, float[] value);

    @Bridge
    public static native void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value);

    @Bridge
    public static native void glUniformMatrix4fv(int location, int count, boolean transpose, float[] value);

    @Bridge
    public static native void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value);

    @Bridge
    public static native void glUseProgram(int program);

    @Bridge
    public static native void glValidateProgram(int program);

    @Bridge
    public static native void glVertexAttrib1f(int index, float v0);

    @Bridge
    public static native void glVertexAttrib2f(int index, float v0, float v1);

    @Bridge
    public static native void glVertexAttrib3f(int index, float v0, float v1, float v2);

    @Bridge
    public static native void glVertexAttrib4f(int index, float v0, float v1, float v2, float v3);

    @Bridge
    public static native void glVertexAttrib1fv(int index, float[] v);

    @Bridge
    public static native void glVertexAttrib1fv(int index, FloatBuffer v);

    @Bridge
    public static native void glVertexAttrib2fv(int index, float[] v);

    @Bridge
    public static native void glVertexAttrib2fv(int index, FloatBuffer v);

    @Bridge
    public static native void glVertexAttrib3fv(int index, float[] v);

    @Bridge
    public static native void glVertexAttrib3fv(int index, FloatBuffer v);

    @Bridge
    public static native void glVertexAttrib4fv(int index, float[] v);

    @Bridge
    public static native void glVertexAttrib4fv(int index, FloatBuffer v);

    @Bridge
    public static native void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, int offset);

    @Bridge
    public static native void glViewport(int x, int y, int width, int height);
}
