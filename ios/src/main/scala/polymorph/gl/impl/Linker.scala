package polymorph.gl.impl

import polymorph.gl.BufferUtils
import polymorph.ui.RootGLView
import polymorph.ui.impl.RootGLViewImpl

/**
 * Created by Matt on 31/07/2015.
 */
object Linker {
  def link(): Unit = {
    BufferUtils.impl = new BufferUtilsImpl
    RootGLView._peer = RootGLViewImpl.apply
    polymorph.Platform._supportsOpenGL = true
  }
}
