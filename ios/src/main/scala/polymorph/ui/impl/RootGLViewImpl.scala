package polymorph.ui.impl

import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.foundation.{NSObject, NSOperationQueue}
import org.robovm.apple.glkit.{GLKViewDrawableStencilFormat, GLKView, GLKViewDelegate, GLKViewController}
import org.robovm.apple.opengles.{EAGLRenderingAPI, EAGLContext}
import org.robovm.apple.uikit._
import polymorph.Prelude
import polymorph.event.impl.{KeyEventReceiverProxy, TouchEventReceiverProxy}

import polymorph.gl.GL
import polymorph.gl.impl.GLES
import polymorph.ui.{RootGLViewPeer, RootGLView}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 31/07/2015.
 */
class RootGLViewImpl (view: RootGLView, _title: String, withId: String) extends RootGLViewPeer(view) {
  val context: EAGLContext = new EAGLContext(EAGLRenderingAPI.OpenGLES2)
  context.setMultiThreaded(true)

  var initialized = false
  val delegate = new NSObject with GLKViewDelegate {
    override def draw(glkView: GLKView, cgRect: CGRect): Unit = {
      if(!initialized) {
        view.init(gl)
        initialized = true
      }
      view.paint(gl)
    }
  }

  var _peer = new GLKView(UIScreen.getMainScreen.getBounds, context)
  val _window = new UIWindow(UIScreen.getMainScreen.getBounds)
  var gl: GL = new GLES
  _peer.setDelegate(delegate)

  _peer.setEnablesSetNeedsDisplay(true)
  _peer.setBackgroundColor(UIColor.white())


  def width: Float =
    (_peer.getBounds.getWidth*UIScreen.getMainScreen.getScale).toFloat

  def visible_=(visible: Boolean): Unit =
    _peer.setHidden(!visible)

  def visible: Boolean =
    !_peer.isHidden

  def title: String = ""

  def height: Float =
    (_peer.getBounds.getHeight.toFloat*UIScreen.getMainScreen.getScale).toFloat

  def title_=(title: String): Unit = {}

  def globalPosition: Vec2 =
    Vec2(0,0)

  def repaint(): Unit =
    NSOperationQueue.getMainQueue.addOperation(new Runnable() {
      override def run(): Unit =
        _peer.setNeedsDisplay()
    })

  class CanvasController extends GLKViewController with TouchEventReceiverProxy {
    val receiver = RootGLViewImpl.this.view
    val view = _peer
    view.setDrawableStencilFormat(GLKViewDrawableStencilFormat._8)
    override def viewWillAppear(animated: Boolean): Unit = {
      super.viewWillAppear(animated)
      setPreferredFramesPerSecond(60)
      setPaused(false)
      setPausesOnWillResignActive(false)
      setResumesOnDidBecomeActive(false)

    }

    override def viewDidAppear(animated: Boolean): Unit =
      view.setNeedsDisplay()
  }
  val controller = new CanvasController
  controller.setView(_peer)

  _window.setRootViewController(controller)

  def focus(): Unit =
    keyboard.becomeFirstResponder()

  def blur(): Unit =
    keyboard.resignFirstResponder()

  val keyboard = new KeyEventReceiverProxy(view)

  _window.addSubview(keyboard)


  def withGL(body: GL => Unit): Unit =
    NSOperationQueue.getMainQueue.addOperation(new Runnable() {
      override def run(): Unit =
        body(gl)
    })


  private var _animate = false
  def animate = _animate
  def animate_=(b: Boolean): Unit = {
    _animate = b
    NSOperationQueue.getMainQueue.addOperation(new Runnable() {
      override def run(): Unit =
        if(b) {
          controller.setPaused(false)
          controller.setPausesOnWillResignActive(true)
          controller.setResumesOnDidBecomeActive(true)
        } else {
          controller.setPaused(true)
          controller.setPausesOnWillResignActive(false)
          controller.setResumesOnDidBecomeActive(false)
        }
    })
  }
  _window.makeKeyAndVisible()

}

object RootGLViewImpl {
  def apply(view: RootGLView, title: String, withId: String): RootGLViewPeer =
    new RootGLViewImpl(view, title, withId)
}
