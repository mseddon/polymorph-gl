package polymorph.gl.impl

import polymorph.gfx._
import polymorph.gfx.impl._
import polymorph.gl.{GL => NGL, _}
import java.nio._
import org.scalajs.dom.ext.Image
import org.scalajs.dom.ext._
import org.scalajs.dom.raw._
import org.scalajs.dom.raw.{ WebGLRenderingContext => GL }
import scalajs.js
import scalajs.js.JSConverters._
import scala.language.implicitConversions
import scala.scalajs.js.typedarray._
import TypedArrayBufferOps._

/**
 * Used to allocate a unique id to each program, shader, buffer etc. in order to yield vaguely useful toStrings that
 * allow one to distinguish between the elements.
 */
private object WebGLCounters {
  var programId = 1
  var shaderId = 1
  var bufferId = 1
  var framebufferId = 1
  var renderbufferId = 1
  var textureId = 1
}

private class WGLProgram(val id: WebGLProgram) extends GLProgram {
  val _id = WebGLCounters.programId
  WebGLCounters.programId += 1
  override def toString = s"GLProgram@${_id}"
}


private class WGLShader(val id: WebGLShader) extends GLShader {
  val _id = WebGLCounters.shaderId
  WebGLCounters.shaderId += 1
  override def toString = s"GLShader@${_id}"
}

private class WGLBuffer(val id: WebGLBuffer) extends GLBuffer {
  val _id = WebGLCounters.bufferId
  WebGLCounters.bufferId += 1
  override def toString = s"GLBuffer@${_id}"
}

private class WGLFramebuffer(val id: WebGLFramebuffer) extends GLFramebuffer {
  val _id = WebGLCounters.framebufferId
  WebGLCounters.framebufferId += 1
  override def toString = s"GLFramebuffer@${_id}"
}

private class WGLRenderbuffer(val id: WebGLRenderbuffer) extends GLRenderbuffer {
  val _id = WebGLCounters.renderbufferId
  WebGLCounters.renderbufferId += 1
  override def toString = s"GLRenderbuffer@${_id}"
}

private class WGLTexture(val id: WebGLTexture) extends GLTexture {
  val _id = WebGLCounters.textureId
  WebGLCounters.textureId += 1
  override def toString = s"GLTexture@${_id}"
}
private class WGLUniformLocation(val id: WebGLUniformLocation) extends GLUniformLocation

private [polymorph] final class WGL(var gl: GL) extends NGL {
  private var glShaders = Seq[GLShader]()
  private var glBuffers = Seq[GLBuffer]()
  private var glTextures = Seq[GLTexture]()
  private var glFramebuffers = Seq[GLFramebuffer]()
  private var glRenderbuffers = Seq[GLRenderbuffer]()
  private var glPrograms = Seq[GLProgram]()

  private implicit def programImp(program: GLProgram): WGLProgram =
    program.asInstanceOf[WGLProgram]
  private implicit def shaderImp(shader: GLShader): WGLShader =
    shader.asInstanceOf[WGLShader]
  private implicit def bufferImp(buffer: GLBuffer): WGLBuffer =
    buffer.asInstanceOf[WGLBuffer]
  private implicit def renderbufferImp(renderbuffer: GLRenderbuffer): WGLRenderbuffer =
    renderbuffer.asInstanceOf[WGLRenderbuffer]
  private implicit def frameBufferImp(framebuffer: GLFramebuffer): WGLFramebuffer =
    framebuffer.asInstanceOf[WGLFramebuffer]
  private implicit def textureImpl(texture: GLTexture): WGLTexture =
    texture.asInstanceOf[WGLTexture]
  private implicit def uniformImpl(uniform: GLUniformLocation): WGLUniformLocation =
    uniform.asInstanceOf[WGLUniformLocation]

  private implicit def byteBufferToArrayBufferView(data: ByteBuffer): ArrayBufferView =
    if(data == null) null
    else new Uint8Array(data.arrayBuffer().slice(data.position, data.limit))

  private implicit def floatBufferToFloat32Array(data: FloatBuffer): Float32Array =
    if(data == null) null
    else new Float32Array(data.arrayBuffer(), data.position*4, data.limit*4)

  private implicit def intBufferToInt32Array(data: IntBuffer): Int32Array =
    if(data == null) null
    else new Int32Array(data.arrayBuffer(), data.position*4, data.limit*4)

  private implicit def bufferToSlice(data: Buffer): ArrayBufferView =
    new Uint8Array(typedArraySlice(data))

  private def typedArraySlice(buffer: Buffer): ArrayBuffer = {
    val typedArray = buffer.arrayBuffer()
    val sz = BufferUtils.size(buffer)
    typedArray.slice(buffer.position()*sz, buffer.limit()*sz)
  }

  @inline
  def activeTexture(texture: Int): Unit =
    gl.activeTexture(texture)

  @inline
  def attachShader(program: GLProgram, shader: GLShader): Unit =
    gl.attachShader(program.id, shader.id)

  @inline
  def bindAttribLocation(program: GLProgram, index: Int, name: String): Unit =
    gl.bindAttribLocation(program.id, index, name)

  @inline
  def bindBuffer(target: Int, buffer: GLBuffer): Unit =
    gl.bindBuffer(target, buffer.id)

  @inline
  def bindFramebuffer(target: Int, framebuffer: GLFramebuffer): Unit =
    gl.bindFramebuffer(target, framebuffer.id)

  @inline
  def bindRenderbuffer(target: Int, renderbuffer: GLRenderbuffer): Unit =
    gl.bindRenderbuffer(target, renderbuffer.id)

  @inline
  def bindTexture(target: Int, texture: GLTexture): Unit =
    gl.bindTexture(target, texture.id)

  @inline
  def blendColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    gl.blendColor(red, green, blue, alpha)

  @inline
  def blendEquation(mode: Int): Unit =
    gl.blendEquation(mode)

  @inline
  def blendEquationSeparate(modeRGB: Int, modeAlpha: Int): Unit =
    gl.blendEquationSeparate(modeRGB, modeAlpha)

  @inline
  def blendFunc(sfactor: Int, dfactor: Int): Unit =
    gl.blendFunc(sfactor, dfactor)

  @inline
  def blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int): Unit =
    gl.blendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)

  @inline
  def bufferData(target: Int, size: Int, usage: Int): Unit =
    gl.bufferData(target, size, usage)

  @inline
  def bufferData(target: Int, data: Buffer, usage: Int): Unit = {
    gl.bufferData(target, data, usage)
  }

  @inline
  def bufferSubData(target: Int, offset: Int, data: Buffer): Unit =
    gl.bufferSubData(target, offset, data)

  @inline
  def clear(mask: Int): Unit =
    gl.clear(mask)

  @inline
  def clearColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    gl.clearColor(red, green, blue, alpha)

  @inline
  def clearDepth(depth: Int): Unit =
    gl.clearDepth(depth)

  @inline
  def clearStencil(s: Int): Unit =
    gl.clearStencil(s)

  @inline
  def colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean): Unit =
    gl.colorMask(red, green, blue, alpha)

  @inline
  def compileShader(shader: GLShader): Unit =
    gl.compileShader(shader.id)

  @inline
  def compressedTexImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, data: ByteBuffer): Unit =
    gl.compressedTexImage2D(target, level, internalFormat, width, height, border, data)

  @inline
  def compressedTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, data: ByteBuffer): Unit =
    gl.compressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, data)

  @inline
  def copyTexImage2D(target: Int, level: Int, internalFormat: Int, x: Int, y: Int, width: Int, height: Int, border: Int): Unit =
    gl.copyTexImage2D(target, level, internalFormat, x, y, width, height, border)

  @inline
  def copyTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, x: Int, y: Int, width: Int, height: Int): Unit =
    gl.copyTexSubImage2D(target, level, xOffset, yOffset, x, y, width, height)

  @inline
  def createBuffer(): GLBuffer = {
    val buffer = new WGLBuffer(gl.createBuffer())
    glBuffers :+= buffer
    buffer
  }

  @inline
  def createFramebuffer(): GLFramebuffer = {
    val framebuffer = new WGLFramebuffer(gl.createFramebuffer())
    glFramebuffers :+= framebuffer
    framebuffer
  }

  @inline
  def createProgram(): GLProgram = {
    val program = new WGLProgram(gl.createProgram())
    glPrograms :+= program
    program
  }

  @inline
  def createRenderbuffer(): GLRenderbuffer = {
    val renderbuffer = new WGLRenderbuffer(gl.createRenderbuffer())
    glRenderbuffers :+= renderbuffer
    renderbuffer
  }

  @inline
  def createShader(`type`: Int): GLShader = {
    val shader = new WGLShader(gl.createShader(`type`))
    glShaders :+= shader
    shader
  }

  @inline
  def createTexture(): GLTexture = {
    val texture = new WGLTexture(gl.createTexture())
    glTextures :+= texture
    texture
  }

  @inline
  def cullFace(mode: Int): Unit =
    gl.cullFace(mode)

  @inline
  def deleteBuffer(buffer: GLBuffer): Unit = {
    glBuffers = glBuffers.filter(_ != buffer)
    gl.deleteBuffer(buffer.id)
  }

  @inline
  def deleteFramebuffer(framebuffer: GLFramebuffer): Unit = {
    glFramebuffers = glFramebuffers.filter(_ != framebuffer)
    gl.deleteFramebuffer(framebuffer.id)
  }

  @inline
  def deleteProgram(program: GLProgram): Unit = {
    glPrograms = glPrograms.filter(_ != program)
    gl.deleteProgram(program.id)
  }

  @inline
  def deleteRenderbuffer(renderbuffer: GLRenderbuffer): Unit = {
    glRenderbuffers = glRenderbuffers.filter(_ != renderbuffer)
    gl.deleteRenderbuffer(renderbuffer.id)
  }

  @inline
  def deleteShader(shader: GLShader): Unit = {
    glShaders = glShaders.filter(_ != shader)
    gl.deleteShader(shader.id)
  }

  @inline
  def deleteTexture(texture: GLTexture): Unit = {
    glTextures = glTextures.filter(_ != texture)
    gl.deleteTexture(texture.id)
  }

  @inline
  def depthFunc(func: Int): Unit =
    gl.depthFunc(func)

  @inline
  def depthMask(flag: Boolean): Unit =
    gl.depthMask(flag)

  @inline
  def depthRange(zNear: Float, zFar: Float): Unit =
    gl.depthRange(zNear, zFar)

  @inline
  def detachShader(program: GLProgram, shader: GLShader): Unit =
    gl.detachShader(program.id, shader.id)

  @inline
  def disable(cap: Int): Unit =
    gl.disable(cap)

  @inline
  def disableVertexAttribArray(index: Int): Unit =
    gl.disableVertexAttribArray(index)

  @inline
  def drawArrays(mode: Int, first: Int, count: Int): Unit =
    gl.drawArrays(mode, first, count)

  @inline
  def drawElements(mode: Int, count: Int, `type`: Int, offset: Int): Unit =
    gl.drawElements(mode, count, `type`, offset)

  @inline
  def enable(cap: Int): Unit =
    gl.enable(cap)

  @inline
  def enableVertexAttribArray(index: Int): Unit =
    gl.enableVertexAttribArray(index)

  @inline
  def finish(): Unit =
    gl.finish()

  @inline
  def flush(): Unit =
    gl.flush()

  @inline
  def framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: GLRenderbuffer): Unit =
    gl.framebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer.id)

  @inline
  def framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: GLTexture, level: Int): Unit =
    gl.framebufferTexture2D(target, attachment, textarget, texture.id, level)

  @inline
  def frontFace(mode: Int): Unit =
    gl.frontFace(mode)

  @inline
  def generateMipmap(target: Int): Unit =
    gl.generateMipmap(target)

  @inline
  def getActiveAttrib(program: GLProgram, index: Int): GLActiveInfo = {
    val attrib = gl.getActiveAttrib(program.id, index)
    new GLActiveInfo(attrib.size, attrib.`type`, attrib.name)
  }

  @inline
  def getActiveUniform(program: GLProgram, index: Int): GLActiveInfo = {
    val uniform = gl.getActiveUniform(program.id, index)
    new GLActiveInfo(uniform.size, uniform.`type`, uniform.name)
  }

  @inline
  def getAttachedShaders(program: GLProgram): Seq[GLShader] = {
    gl.getAttachedShaders(program.id).map(x => glShaders.find(_.id == x).orNull)
  }

  @inline
  def getAttribLocation(program: GLProgram, name: String): Int =
    gl.getAttribLocation(program.id, name)

  @inline
  def getBufferParameter(target: Int, pname: Int): Int =
    gl.getBufferParameter(target, pname)

  @inline
  def getParameter(param: Int): Any =
    gl.getParameter(param)

  @inline
  def getError(): Int =
    gl.getError()

  @inline
  def getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any = {
    val output = gl.getFramebufferAttachmentParameter(target, attachment, pname)
    if(pname == NGL.FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE)
      glRenderbuffers.find(_.id == output.asInstanceOf[WebGLRenderbuffer]).get
    else
      output
  }

  @inline
  def getProgramParameter(program: GLProgram, pname: Int): Any =
    gl.getProgramParameter(program.id, pname)

  @inline
  def getProgramInfoLog(program: GLProgram): String =
    gl.getProgramInfoLog(program.id)

  @inline
  def getRenderbufferParameter(target: Int, pname: Int): Int =
    gl.getRenderbufferParameter(target, pname).asInstanceOf[Int]

  @inline
  def getShaderParameter(shader: GLShader, pname: Int): Any =
    gl.getShaderParameter(shader.id, pname)

  @inline
  def getShaderPrecisionFormat(shaderType: Int, precisionType: Int): GLShaderPrecisionFormat = {
    val precision = gl.getShaderPrecisionFormat(shaderType, precisionType)
    new GLShaderPrecisionFormat(precision.rangeMin, precision.rangeMax, precision.precision)
  }

  @inline
  def getShaderInfoLog(shader: GLShader): String =
    gl.getShaderInfoLog(shader.id)

  @inline
  def getShaderSource(shader: GLShader): String =
    gl.getShaderSource(shader.id)

  @inline
  def getTexParameter(target: Int, pname: Int): Int =
    gl.getTexParameter(target, pname).asInstanceOf[Int]

  //  def getUniform(program: GLProgram, location: GLUniformLocation): Any

  @inline
  def getUniformLocation(program: GLProgram, name: String): GLUniformLocation =
    new WGLUniformLocation(gl.getUniformLocation(program.id, name))

  def getVertexAttrib(index: Int, pname: Int): Any = {
    val out = gl.getVertexAttrib(index, pname)
    pname match {
      case NGL.VERTEX_ATTRIB_ARRAY_BUFFER_BINDING =>
        glBuffers.find(_.id == out.asInstanceOf[WebGLBuffer]).orNull
      case _ =>
        out
    }
  }

  @inline
  def hint(target: Int, mode: Int): Unit =
    gl.hint(target, mode)

  @inline
  def isBuffer(buffer: GLBuffer): Boolean =
    buffer != null && gl.isBuffer(buffer.id)

  @inline
  def isEnabled(cap: Int): Boolean =
    gl.isEnabled(cap)

  @inline
  def isFramebuffer(framebuffer: GLFramebuffer): Boolean =
    framebuffer != null && gl.isFramebuffer(framebuffer.id)

  @inline
  def isProgram(program: GLProgram): Boolean =
    program != null && gl.isProgram(program.id)

  @inline
  def isRenderbuffer(renderbuffer: GLRenderbuffer): Boolean =
    renderbuffer != null && gl.isRenderbuffer(renderbuffer.id)

  @inline
  def isShader(shader: GLShader): Boolean =
    shader != null && gl.isShader(shader.id)

  @inline
  def isTexture(texture: GLTexture): Boolean =
    texture != null && gl.isTexture(texture.id)

  @inline
  def lineWidth(width: Float): Unit =
    gl.lineWidth(width)

  @inline
  def linkProgram(program: GLProgram): Unit =
    gl.linkProgram(program.id)

  @inline
  def pixelStorei(pname: Int, param: Int): Unit =
    gl.pixelStorei(pname, param)

  @inline
  def polygonOffset(factor: Float, units: Float): Unit =
    gl.polygonOffset(factor, units)

  @inline
  def readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.readPixels(x, y, width, height, format, `type`, pixels)

  @inline
  def renderbufferStorage(target: Int, internalFormat: Int, width: Int, height: Int): Unit =
    gl.renderbufferStorage(target, internalFormat, width, height)

  @inline
  def sampleCoverage(value: Int, invert: Boolean): Unit =
    gl.sampleCoverage(value, invert)

  @inline
  def scissor(x: Int, y: Int, width: Int, height: Int): Unit =
    gl.scissor(x, y, width, height)

  @inline
  def shaderSource(shader: GLShader, source: String): Unit =
    gl.shaderSource(shader.id, source)

  @inline
  def stencilFunc(func: Int, ref: Int, mask: Int): Unit =
    gl.stencilFunc(func, ref, mask)

  @inline
  def stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int): Unit =
    gl.stencilFuncSeparate(face, func, ref, mask)

  @inline
  def stencilMask(mask: Int): Unit =
    gl.stencilMask(mask)

  @inline
  def stencilMaskSeparate(face: Int, mask: Int): Unit =
    gl.stencilMaskSeparate(face, mask)

  @inline
  def stencilOp(fail: Int, zFail: Int, zPass: Int): Unit =
    gl.stencilOp(fail, zFail, zPass)

  @inline
  def stencilOpSeparate(face: Int, fail: Int, zFail: Int, zPass: Int): Unit =
    gl.stencilOpSeparate(face, fail, zFail, zPass)

  @inline
  def texImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.texImage2D(target, level, internalFormat, width, height, border, format, `type`, pixels)

  @inline
  def texImage2D(target: Int, level: Int, internalFormat: Int, format: Int, `type`: Int, pixels: Image): Unit = {
    gl.pixelStorei(WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1)
    gl.texImage2D(target, level, internalFormat, format, `type`, pixels.asInstanceOf[ImageImpl].peer)
    gl.pixelStorei(WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 0)
  }

  //  def texImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: Image): Unit

  @inline
  def texParameterf(target: Int, pname: Int, param: Float): Unit =
    gl.texParameterf(target, pname, param)

  @inline
  def texParameteri(target: Int, pname: Int, param: Int): Unit =
    gl.texParameteri(target, pname, param)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.texSubImage2D(target, level, xOffset, yOffset, width, height, format, `type`, pixels)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, format: Int, `type`: Int, pixels: Image): Unit = {
    gl.pixelStorei(WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1)
    gl.texSubImage2D(target, level, xOffset, yOffset, format, `type`, pixels.asInstanceOf[ImageImpl].peer)
    gl.pixelStorei(WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 0)
  }

  //  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: Image): Unit

  @inline
  def uniform1f(location: GLUniformLocation, x: Float): Unit =
    gl.uniform1f(location.id, x)

  @inline
  def uniform1fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.uniform1fv(location.id, v)

  @inline
  def uniform1fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.uniform1fv(location.id, v.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniform1i(location: GLUniformLocation, x: Int): Unit =
    gl.uniform1i(location.id, x)

  @inline
  def uniform1iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.uniform1iv(location.id, v)

  @inline
  def uniform1iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.uniform1iv(location.id, v.toJSArray)

  @inline
  def uniform2f(location: GLUniformLocation, x: Float, y: Float): Unit =
    gl.uniform2f(location.id, x, y)

  @inline
  def uniform2fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.uniform2fv(location.id, v)

  @inline
  def uniform2fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.uniform2fv(location.id, v.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniform2i(location: GLUniformLocation, x: Int, y: Int): Unit =
    gl.uniform2i(location.id, x, y)

  @inline
  def uniform2iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.uniform2iv(location.id, v)

  @inline
  def uniform2iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.uniform2iv(location.id, v.toJSArray)

  @inline
  def uniform3f(location: GLUniformLocation, x: Float, y: Float, z: Float): Unit =
    gl.uniform3f(location.id, x, y, z)

  @inline
  def uniform3fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.uniform3fv(location.id, v)

  @inline
  def uniform3fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.uniform3fv(location.id, v.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniform3i(location: GLUniformLocation, x: Int, y: Int, z: Int): Unit =
    gl.uniform3i(location.id, x, y, z)

  @inline
  def uniform3iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.uniform3iv(location.id, v)

  @inline
  def uniform3iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.uniform3iv(location.id, v.toJSArray)

  @inline
  def uniform4f(location: GLUniformLocation, x: Float, y: Float, z: Float, w: Float): Unit =
    gl.uniform4f(location.id, x, y, z, w)

  @inline
  def uniform4fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.uniform4fv(location.id, v)

  @inline
  def uniform4fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.uniform4fv(location.id, v.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniform4i(location: GLUniformLocation, x: Int, y: Int, z: Int, w: Int): Unit =
    gl.uniform4i(location.id, x, y, z, w)

  @inline
  def uniform4iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.uniform4iv(location.id, v)

  @inline
  def uniform4iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.uniform4iv(location.id, v.toJSArray)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.uniformMatrix2fv(location.id, transpose, value)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.uniformMatrix2fv(location.id, transpose, value.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.uniformMatrix3fv(location.id, transpose, value)

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.uniformMatrix3fv(location.id, transpose, value.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.uniformMatrix4fv(location.id, transpose, value)

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.uniformMatrix4fv(location.id, transpose, value.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def useProgram(program: GLProgram): Unit =
    gl.useProgram(program.id)

  @inline
  def validateProgram(program: GLProgram): Unit =
    gl.validateProgram(program.id)

  @inline
  def vertexAttrib1f(indx: Int, x: Float): Unit =
    gl.vertexAttrib1f(indx, x)

  @inline
  def vertexAttrib1fv(indx: Int, values: FloatBuffer): Unit =
    gl.vertexAttrib1fv(indx, values)

  @inline
  def vertexAttrib1fv(indx: Int, values: Array[Float]): Unit =
    gl.vertexAttrib1fv(indx, values.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def vertexAttrib2f(indx: Int, x: Float, y: Float): Unit =
    gl.vertexAttrib2f(indx, x, y)

  @inline
  def vertexAttrib2fv(indx: Int, values: FloatBuffer): Unit =
    gl.vertexAttrib2fv(indx, values)

  @inline
  def vertexAttrib2fv(indx: Int, values: Array[Float]): Unit =
    gl.vertexAttrib2fv(indx, values.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def vertexAttrib3f(indx: Int, x: Float, y: Float, z: Float): Unit =
    gl.vertexAttrib3f(indx, x, y, z)

  @inline
  def vertexAttrib3fv(indx: Int, values: FloatBuffer): Unit =
    gl.vertexAttrib3fv(indx, values)

  @inline
  def vertexAttrib3fv(indx: Int, values: Array[Float]): Unit =
    gl.vertexAttrib3fv(indx, values.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def vertexAttrib4f(indx: Int, x: Float, y: Float, z: Float, w: Float): Unit =
    gl.vertexAttrib4f(indx, x, y, z, w)

  @inline
  def vertexAttrib4fv(indx: Int, values: FloatBuffer): Unit =
    gl.vertexAttrib4fv(indx, values)

  @inline
  def vertexAttrib4fv(indx: Int, values: Array[Float]): Unit =
    gl.vertexAttrib4fv(indx, values.toJSArray.asInstanceOf[js.Array[Double]])

  @inline
  def vertexAttribPointer(indx: Int, size: Int, `type`: Int, normalized: Boolean, stride: Int, offset: Int): Unit =
    gl.vertexAttribPointer(indx, size, `type`, normalized, stride, offset)

  @inline
  def viewport(x: Int, y: Int, width: Int, height: Int): Unit =
    gl.viewport(x, y, width, height)
}

