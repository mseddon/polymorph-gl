package polymorph.ui.impl

import org.scalajs.dom
import polymorph.event.impl.{KeyEventReceiverProxy, TouchEventReceiverProxy, MouseEventReceiverProxy}
import polymorph.gl.GL
import polymorph.gl.impl.WGL
import polymorph.ui.{RootGLViewPeer, RootGLView}
import scryetek.vecmath.Vec2

class RootGLViewImpl(view: RootGLView, _title: String, withId: String) extends RootGLViewPeer(view) {
  val _peer: dom.html.Canvas = if(withId == null) {
    val canvas = dom.document.createElement("canvas").asInstanceOf[dom.html.Canvas]
    canvas.width = 800
    canvas.height = 800
    dom.document.body.appendChild(canvas)
    canvas
  } else {
    dom.document.getElementById(withId).asInstanceOf[dom.html.Canvas]
  }

  val _mouseReceiverProxy = new MouseEventReceiverProxy(_peer, view)
  val _touchReceiverProxy = new TouchEventReceiverProxy(_peer, view)

  val gl = {
    val opts = scalajs.js.Dynamic.literal(stencil = true)
    new WGL(
    Option(_peer.getContext("webgl", opts)).orElse(Option(_peer.getContext("webgl-experimental", opts))).getOrElse(
      throw new Error("GL Unsupported")
    ).asInstanceOf[dom.raw.WebGLRenderingContext])
  }

  def globalPosition = {
    val rect = _peer.getBoundingClientRect()
    Vec2((rect.left + dom.document.body.scrollLeft).toFloat, (rect.top + dom.document.body.scrollTop).toFloat)
  }

  def width: Float =
    _peer.clientWidth

  def height: Float =
    _peer.clientHeight

  def title: String =
    _peer.getAttribute("alt")

  def title_=(title: String) =
    _peer.setAttribute("alt", title)

  def visible: Boolean =
    true

  def visible_=(visible: Boolean) = {}

  private var pendingRepaint = false

  def repaint(): Unit = {
    if(pendingRepaint)
      return
    pendingRepaint = true
    dom.window.requestAnimationFrame { t: Double =>
      pendingRepaint = false
      view.paint(gl)
      if(_animate)
        repaint()
    }
  }

  def focus(): Unit = {
    KeyEventReceiverProxy.keyboard.focus()
    KeyEventReceiverProxy.receiver = view
  }

  def blur(): Unit = {
    KeyEventReceiverProxy.keyboard.blur()
    KeyEventReceiverProxy.receiver = null
  }

  def withGL(body: GL => Unit): Unit =
    body(gl)

  private var _animate = false

  /** Returns if the GLApp is currently animating */
  def animate = _animate

  /** Make the GLApp animate continuously or not. */
  def animate_=(b: Boolean): Unit = {
    _animate = b
    if(_animate)
      view.repaint()
  }

  view.init(gl)

  repaint()
}

object RootGLViewImpl {
  def apply(view: RootGLView, _title: String, withId: String): RootGLViewPeer =
    new RootGLViewImpl(view, _title, withId)
}
