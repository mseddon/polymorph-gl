package polymorph.gl.impl
import polymorph.ui._
import polymorph.gl._
import polymorph.ui.impl.RootGLViewImpl

object Linker {
  def link(): Unit = {
    BufferUtils.impl = new BufferUtilsImpl
    RootGLView._peer = RootGLViewImpl.apply
    polymorph.Platform._supportsOpenGL = true
  }
}