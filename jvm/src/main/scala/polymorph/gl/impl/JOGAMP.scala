package polymorph.gl.impl

import java.nio.{Buffer, ByteBuffer, FloatBuffer, IntBuffer}
import com.jogamp.opengl._
import polymorph.gfx.impl._
import polymorph.gfx._
import polymorph.gl.{ GL => NGL, _}

import scala.language.implicitConversions

private class JOGAMPProgram(val id: Int) extends GLProgram {
  override def toString = s"GLProgram@$id"
}

private class JOGAMPShader(val id: Int) extends GLShader {
  override def toString = s"GLShader@$id"
}

private class JOGAMPBuffer(val id: Int) extends GLBuffer {
  override def toString = s"GLBuffer@$id"
}

private class JOGAMPFramebuffer(val id: Int) extends GLFramebuffer {
  override def toString = s"GLFramebuffer@$id"
}

private class JOGAMPRenderbuffer(val id: Int) extends GLRenderbuffer {
  override def toString = s"GLRenderbuffer@$id"
}

private class JOGAMPTexture(val id: Int) extends GLTexture {
  override def toString = s"GLTexture@$id"
}

private class JOGAMPUniformLocation(val id: Int) extends GLUniformLocation


private [polymorph] final class JOGAMP(var gl: GL2ES2) extends NGL {
  private val glShaders = collection.mutable.Map[Int, GLShader]()
  private val glBuffers = collection.mutable.Map[Int, GLBuffer]()
  private val glTextures = collection.mutable.Map[Int, GLTexture]()
  private val glFramebuffers = collection.mutable.Map[Int, GLFramebuffer]()
  private val glRenderbuffers = collection.mutable.Map[Int, GLRenderbuffer]()
  private val glPrograms = collection.mutable.Map[Int, GLProgram]()
  private val paramString = (x: Int) => gl.glGetString(x)

  private val paramInt = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    out.get(0)
  }
  private val paramIntN = (n: Int) => {
    (x: Int) => {
      val intBuffer = IntBuffer.allocate(n)
      gl.glGetIntegerv(x, intBuffer)
      intBuffer
    }
  }
  private val paramInt2 = paramIntN(2)
  private val paramInt4 = paramIntN(4)
  private val paramUint = (x: Int) => paramInt

  private val paramBool = (x: Int) => {
    val out = ByteBuffer.allocate(1)
    gl.glGetBooleanv(x, out)
    out.get(0) != 0
  }

  private val paramBoolN = (n: Int) => {
    (x: Int) => {
      val byteBuffer = ByteBuffer.allocate(n)
      gl.glGetBooleanv(x, byteBuffer)
      val outBools = new Array[Boolean](n)
      for(i <- 0 until n)
        outBools(i) = byteBuffer.get(i) != 0
      outBools
    }
  }
  private val paramBool4 = paramBoolN(4)

  private val paramEnum = paramInt
  private val paramFloat = (x: Int) => {
    val out = FloatBuffer.allocate(1)
    gl.glGetFloatv(x, out)
    out.get(0)
  }
  private val paramFloatN = (n: Int) => {
    (x: Int) => {
      val floatBuffer = FloatBuffer.allocate(n)
      gl.glGetFloatv(x, floatBuffer)
      floatBuffer
    }
  }
  private val paramFloat2 = paramFloatN(2)
  private val paramFloat4 = paramFloatN(4)
  private val paramGLBuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    glBuffers.getOrElse(out.get(0), null)
  }
  private val paramGLFramebuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    glFramebuffers.getOrElse(out.get(0), null)
  }
  private val paramGLProgram = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    glPrograms.getOrElse(out.get(0), null)
  }
  private val paramGLRenderbuffer = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    glRenderbuffers.getOrElse(out.get(0), null)
  }
  private val paramGLTexture = (x: Int) => {
    val out = IntBuffer.allocate(1)
    gl.glGetIntegerv(x, out)
    glTextures.getOrElse(out.get(0), null)
  }

  private val parameters = Map[Int, (Int) => Any](
    NGL.ACTIVE_TEXTURE -> paramEnum,
    NGL.ALIASED_LINE_WIDTH_RANGE -> paramFloat2,
    NGL.ALIASED_POINT_SIZE_RANGE -> paramFloat2,
    NGL.ALPHA_BITS -> paramInt,
    NGL.ARRAY_BUFFER_BINDING -> paramGLBuffer,
    NGL.BLEND -> paramBool,
    NGL.BLEND_COLOR -> paramFloat4,
    NGL.BLEND_DST_ALPHA -> paramEnum,
    NGL.BLEND_EQUATION_ALPHA -> paramEnum,
    NGL.BLEND_EQUATION_RGB -> paramEnum,
    NGL.BLEND_SRC_ALPHA -> paramEnum,
    NGL.BLEND_SRC_RGB -> paramEnum,
    NGL.BLUE_BITS -> paramInt,
    NGL.COLOR_CLEAR_VALUE -> paramFloat4,
    NGL.COLOR_WRITEMASK -> paramBool4,
//    NGL.COMPRESSED_TEXTURE_FORMATS -> ???,
    NGL.CULL_FACE -> paramBool,
    NGL.CULL_FACE_MODE -> paramEnum,
    NGL.CURRENT_PROGRAM -> paramGLProgram,
    NGL.DEPTH_BITS -> paramInt,
    NGL.DEPTH_CLEAR_VALUE -> paramFloat,
    NGL.DEPTH_FUNC -> paramEnum,
    NGL.DEPTH_RANGE -> paramFloat2,
    NGL.DEPTH_TEST -> paramBool,
    NGL.DEPTH_WRITEMASK -> paramBool,
    NGL.DITHER -> paramBool,
    NGL.ELEMENT_ARRAY_BUFFER_BINDING -> paramGLBuffer,
    NGL.FRAMEBUFFER_BINDING -> paramGLFramebuffer,
    NGL.FRONT_FACE -> paramEnum,
    NGL.GENERATE_MIPMAP_HINT -> paramEnum,
    NGL.GREEN_BITS -> paramInt,
    NGL.IMPLEMENTATION_COLOR_READ_FORMAT -> paramEnum,
    NGL.IMPLEMENTATION_COLOR_READ_TYPE -> paramEnum,
    NGL.LINE_WIDTH -> paramFloat,
    NGL.MAX_COMBINED_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_CUBE_MAP_TEXTURE_SIZE -> paramInt,
    NGL.MAX_FRAGMENT_UNIFORM_VECTORS -> paramInt,
    NGL.MAX_RENDERBUFFER_SIZE -> paramInt,
    NGL.MAX_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_TEXTURE_SIZE -> paramInt,
    NGL.MAX_VARYING_VECTORS -> paramInt,
    NGL.MAX_VERTEX_ATTRIBS -> paramInt,
    NGL.MAX_VERTEX_TEXTURE_IMAGE_UNITS -> paramInt,
    NGL.MAX_VERTEX_UNIFORM_VECTORS -> paramInt,
    NGL.MAX_VIEWPORT_DIMS -> paramInt2,
    NGL.PACK_ALIGNMENT -> paramInt,
    NGL.POLYGON_OFFSET_FACTOR -> paramFloat,
    NGL.POLYGON_OFFSET_FILL -> paramBool,
    NGL.POLYGON_OFFSET_UNITS -> paramFloat,
    NGL.RED_BITS -> paramInt,
    NGL.RENDERBUFFER_BINDING -> paramGLRenderbuffer,
    NGL.RENDERER -> paramString,
    NGL.SAMPLE_BUFFERS -> paramInt,
    NGL.SAMPLE_COVERAGE_INVERT -> paramBool,
    NGL.SAMPLE_COVERAGE_VALUE -> paramFloat,
    NGL.SAMPLES -> paramInt,
    NGL.SCISSOR_BOX -> paramInt4,
    NGL.SCISSOR_TEST -> paramBool,
    NGL.SHADING_LANGUAGE_VERSION -> paramString,
    NGL.STENCIL_BACK_FAIL -> paramEnum,
    NGL.STENCIL_BACK_FUNC -> paramEnum,
    NGL.STENCIL_BACK_PASS_DEPTH_FAIL -> paramEnum,
    NGL.STENCIL_BACK_PASS_DEPTH_PASS -> paramEnum,
    NGL.STENCIL_BACK_REF -> paramEnum,
    NGL.STENCIL_BACK_VALUE_MASK -> paramUint,
    NGL.STENCIL_BACK_WRITEMASK -> paramUint,
    NGL.STENCIL_BITS -> paramInt,
    NGL.STENCIL_CLEAR_VALUE -> paramInt,
    NGL.STENCIL_FAIL -> paramEnum,
    NGL.STENCIL_FUNC -> paramEnum,
    NGL.STENCIL_PASS_DEPTH_FAIL -> paramEnum,
    NGL.STENCIL_PASS_DEPTH_PASS -> paramEnum,
    NGL.STENCIL_REF -> paramEnum,
    NGL.STENCIL_TEST -> paramBool,
    NGL.STENCIL_VALUE_MASK -> paramUint,
    NGL.STENCIL_WRITEMASK -> paramUint,
    NGL.SUBPIXEL_BITS -> paramInt,
    NGL.TEXTURE_BINDING_2D -> paramGLTexture,
    NGL.TEXTURE_BINDING_CUBE_MAP -> paramGLTexture,
    NGL.UNPACK_ALIGNMENT -> paramInt,
    //NGL.UNPACK_COLORSPACE_CONVERSION_WEBGL -> paramEnum,
    //NGL.UNPACK_FLIP_Y_WEBGL -> paramEnum,
    //  NGL.UNPACK_PREMULTIPLY_ALPHA_WEBGL -> paramBoolean,
    NGL.VENDOR -> paramString,
    NGL.VERSION -> paramString,
    NGL.VIEWPORT -> paramInt4
  )

  private implicit def programImp(program: GLProgram): JOGAMPProgram =
    program.asInstanceOf[JOGAMPProgram]
  private implicit def shaderImp(shader: GLShader): JOGAMPShader =
    shader.asInstanceOf[JOGAMPShader]
  private implicit def bufferImp(buffer: GLBuffer): JOGAMPBuffer =
    buffer.asInstanceOf[JOGAMPBuffer]
  private implicit def renderbufferImp(renderbuffer: GLRenderbuffer): JOGAMPRenderbuffer =
    renderbuffer.asInstanceOf[JOGAMPRenderbuffer]
  private implicit def frameBufferImp(framebuffer: GLFramebuffer): JOGAMPFramebuffer =
    framebuffer.asInstanceOf[JOGAMPFramebuffer]
  private implicit def textureImpl(texture: GLTexture): JOGAMPTexture =
    texture.asInstanceOf[JOGAMPTexture]
  private implicit def uniformImpl(uniform: GLUniformLocation): JOGAMPUniformLocation =
    uniform.asInstanceOf[JOGAMPUniformLocation]

  @inline
  def activeTexture(texture: Int): Unit =
    gl.glActiveTexture(texture)

  @inline
  def attachShader(program: GLProgram, shader: GLShader): Unit =
    gl.glAttachShader(program.id, shader.id)

  @inline
  def bindAttribLocation(program: GLProgram, index: Int, name: String): Unit =
    gl.glBindAttribLocation(program.id, index, name)

  @inline
  def bindBuffer(target: Int, buffer: GLBuffer): Unit =
    gl.glBindBuffer(target, buffer.id)

  @inline
  def bindFramebuffer(target: Int, framebuffer: GLFramebuffer): Unit =
    gl.glBindFramebuffer(target, framebuffer.id)

  @inline
  def bindRenderbuffer(target: Int, renderbuffer: GLRenderbuffer): Unit =
    gl.glBindRenderbuffer(target, renderbuffer.id)

  @inline
  def bindTexture(target: Int, texture: GLTexture): Unit =
    gl.glBindTexture(target, texture.id)

  @inline
  def blendColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    gl.glBlendColor(red, green, blue, alpha)

  @inline
  def blendEquation(mode: Int): Unit =
    gl.glBlendEquation(mode)

  @inline
  def blendEquationSeparate(modeRGB: Int, modeAlpha: Int): Unit =
    gl.glBlendEquationSeparate(modeRGB, modeAlpha)

  @inline
  def blendFunc(sfactor: Int, dfactor: Int): Unit =
    gl.glBlendFunc(sfactor, dfactor)

  @inline
  def blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int): Unit =
    gl.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)

  @inline
  def bufferData(target: Int, size: Int, usage: Int): Unit =
    gl.glBufferData(target, size, null, usage)

  @inline
  def bufferData(target: Int, data: Buffer, usage: Int): Unit =
    gl.glBufferData(target, data.capacity()*BufferUtils.size(data), data, usage)

  @inline
  def bufferSubData(target: Int, offset: Int, data: Buffer): Unit =
    gl.glBufferSubData(target, offset, data.capacity()*BufferUtils.size(data), data)

  @inline
  def clear(mask: Int): Unit =
    gl.glClear(mask)

  @inline
  def clearColor(red: Float, green: Float, blue: Float, alpha: Float): Unit =
    gl.glClearColor(red, green, blue, alpha)

  @inline
  def clearDepth(depth: Int): Unit =
    gl.glClearDepth(depth)

  @inline
  def clearStencil(s: Int): Unit =
    gl.glClearStencil(s)

  @inline
  def colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean): Unit =
    gl.glColorMask(red, green, blue, alpha)

  @inline
  def compileShader(shader: GLShader): Unit =
    gl.glCompileShader(shader.id)

  @inline
  def compressedTexImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, data: ByteBuffer): Unit =
    gl.glCompressedTexImage2D(target, level, internalFormat, width, height, border, data.remaining(), data)

  @inline
  def compressedTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, data: ByteBuffer): Unit =
    gl.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, data.remaining(), data)

  @inline
  def copyTexImage2D(target: Int, level: Int, internalFormat: Int, x: Int, y: Int, width: Int, height: Int, border: Int): Unit =
    gl.glCopyTexImage2D(target, level, internalFormat, x, y, width, height, border)

  @inline
  def copyTexSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, x: Int, y: Int, width: Int, height: Int): Unit =
    gl.glCopyTexSubImage2D(target, level, xOffset, yOffset, x, y, width, height)

  @inline
  def createBuffer(): GLBuffer = {
    val out = IntBuffer.allocate(1)
    gl.glGenBuffers(1, out)
    val buffer = new JOGAMPBuffer(out.get(0))
    glBuffers.put(buffer.id, buffer)
    buffer
  }

  @inline
  def createFramebuffer(): GLFramebuffer = {
    val out = IntBuffer.allocate(1)
    gl.glGenFramebuffers(1, out)
    val framebuffer = new JOGAMPFramebuffer(out.get(0))
    glFramebuffers.put(framebuffer.id, framebuffer)
    framebuffer
  }

  @inline
  def createProgram(): GLProgram = {
    val program = new JOGAMPProgram(gl.glCreateProgram())
    glPrograms.put(program.id, program)
    program
  }

  @inline
  def createRenderbuffer(): GLRenderbuffer = {
    val out = IntBuffer.allocate(1)
    gl.glGenRenderbuffers(1, out)
    val renderbuffer = new JOGAMPRenderbuffer(out.get(0))
    glRenderbuffers.put(renderbuffer.id, renderbuffer)
    renderbuffer
  }

  @inline
  def createShader(`type`: Int): GLShader = {
    val shader = new JOGAMPShader(gl.glCreateShader(`type`))
    glShaders.put(shader.id, shader)
    shader
  }

  @inline
  def createTexture(): GLTexture = {
    val out = IntBuffer.allocate(1)
    gl.glGenTextures(1, out)
    val texture = new JOGAMPTexture(out.get(0))
    glTextures.put(texture.id, texture)
    texture
  }

  @inline
  def cullFace(mode: Int): Unit =
    gl.glCullFace(mode)

  @inline
  def deleteBuffer(buffer: GLBuffer): Unit = {
    glBuffers.remove(buffer.id)
    gl.glDeleteBuffers(1, Array(buffer.id), 0)
  }

  @inline
  def deleteFramebuffer(framebuffer: GLFramebuffer): Unit = {
    glFramebuffers.remove(framebuffer.id)
    gl.glDeleteFramebuffers(1, Array(framebuffer.id), 0)
  }

  @inline
  def deleteProgram(program: GLProgram): Unit = {
    glPrograms.remove(program.id)
    gl.glDeleteProgram(program.id)
  }

  @inline
  def deleteRenderbuffer(renderbuffer: GLRenderbuffer): Unit = {
    glRenderbuffers.remove(renderbuffer.id)
    gl.glDeleteRenderbuffers(1, Array(renderbuffer.id), 0)
  }

  @inline
  def deleteShader(shader: GLShader): Unit = {
    glShaders.remove(shader.id)
    gl.glDeleteShader(shader.id)
  }

  @inline
  def deleteTexture(texture: GLTexture): Unit = {
    glTextures.remove(texture.id)
    gl.glDeleteTextures(1, Array(texture.id), 0)
  }

  @inline
  def depthFunc(func: Int): Unit =
    gl.glDepthFunc(func)

  @inline
  def depthMask(flag: Boolean): Unit =
    gl.glDepthMask(flag)

  @inline
  def depthRange(zNear: Float, zFar: Float): Unit =
    gl.glDepthRange(zNear, zFar)

  @inline
  def detachShader(program: GLProgram, shader: GLShader): Unit =
    gl.glDetachShader(program.id, shader.id)

  @inline
  def disable(cap: Int): Unit =
    gl.glDisable(cap)

  @inline
  def disableVertexAttribArray(index: Int): Unit =
    gl.glDisableVertexAttribArray(index)

  @inline
  def drawArrays(mode: Int, first: Int, count: Int): Unit =
    gl.glDrawArrays(mode, first, count)

  @inline
  def drawElements(mode: Int, count: Int, `type`: Int, offset: Int): Unit =
    gl.glDrawElements(mode, count, `type`, offset)

  @inline
  def enable(cap: Int): Unit =
    gl.glEnable(cap)

  @inline
  def enableVertexAttribArray(index: Int): Unit =
    gl.glEnableVertexAttribArray(index)

  @inline
  def finish(): Unit =
    gl.glFinish()

  @inline
  def flush(): Unit =
    gl.glFlush()

  @inline
  def framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: GLRenderbuffer): Unit =
    gl.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer.id)

  @inline
  def framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: GLTexture, level: Int): Unit =
    gl.glFramebufferTexture2D(target, attachment, textarget, texture.id, level)

  @inline
  def frontFace(mode: Int): Unit =
    gl.glFrontFace(mode)

  @inline
  def generateMipmap(target: Int): Unit =
    gl.glGenerateMipmap(target)

  @inline
  def getActiveAttrib(program: GLProgram, index: Int): GLActiveInfo = {
    val count = new Array[Int](1)
    val length = IntBuffer.allocate(1)
    val size = IntBuffer.allocate(1)
    val typ = IntBuffer.allocate(1)
    val name = ByteBuffer.allocate(512)
    gl.glGetActiveAttrib(program.id, 512, index, length, size, typ, name)
    new GLActiveInfo(size.get(0), typ.get(0), new String(name.array(), "UTF-8"))
  }

  @inline
  def getActiveUniform(program: GLProgram, index: Int): GLActiveInfo = {
    val count = new Array[Int](1)
    val length = IntBuffer.allocate(1)
    val size = IntBuffer.allocate(1)
    val typ = IntBuffer.allocate(1)
    val name = ByteBuffer.allocate(512)
    gl.glGetActiveUniform(program.id, 512, index, length, size, typ, name)
    new GLActiveInfo(size.get(0), typ.get(0), new String(name.array(), "UTF-8"))
  }

  @inline
  def getAttachedShaders(program: GLProgram): Seq[GLShader] = {
    val count = IntBuffer.allocate(1)
    val shaders = IntBuffer.allocate(256)
    gl.glGetAttachedShaders(program.id, 256, count, shaders)
    (for(i <- 0 until count.get(0)) yield glShaders(shaders.get(i))).toSeq
  }

  @inline
  def getAttribLocation(program: GLProgram, name: String): Int =
    gl.glGetAttribLocation(program.id, name)

  @inline
  def getBufferParameter(target: Int, pname: Int): Int = {
    var out = IntBuffer.allocate(1)
    gl.glGetBufferParameteriv(target, pname, out)
    out.get(0)
  }

  @inline
  def getParameter(param: Int): Any =
    parameters(param)(param)

  @inline
  def getError(): Int =
    gl.glGetError()

  @inline
  def getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any = {
    val output = IntBuffer.allocate(0)
    gl.glGetFramebufferAttachmentParameteriv(target, attachment, pname, output)
    if(pname == NGL.FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE)
      glRenderbuffers.get(output.get(0))
    else
      output.get(0)
  }

  @inline
  def getProgramParameter(program: GLProgram, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    gl.glGetProgramiv(program.id, pname, out)
    if(pname == NGL.DELETE_STATUS || pname == NGL.LINK_STATUS || pname == NGL.VALIDATE_STATUS)
      out.get(0) != 0
    else
      out.get(0)
  }

  @inline
  def getProgramInfoLog(program: GLProgram): String = {
    val lengths = Array[Int](0)
    gl.glGetProgramiv(program.id, GL2ES2.GL_INFO_LOG_LENGTH, lengths, 0)
    if(lengths(0) == 0)
      return ""
    val buf = new Array[Byte](lengths(0))
    gl.glGetProgramInfoLog(program.id, lengths(0), lengths, 0, buf, 0)
    new String(buf, "UTF-8")
  }

  @inline
  def getRenderbufferParameter(target: Int, pname: Int): Int = {
    val out = IntBuffer.allocate(1)
    gl.glGetRenderbufferParameteriv(target, pname, out)
    out.get(0)
  }

  @inline
  def getShaderParameter(shader: GLShader, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    gl.glGetShaderiv(shader.id, pname, out)
    if (pname == NGL.SHADER_TYPE)
      out.get(0)
    else
      out.get(0) != 0
  }

  @inline
  def getShaderPrecisionFormat(shaderType: Int, precisionType: Int): GLShaderPrecisionFormat = {
    val range = IntBuffer.allocate(2)
    val precision = IntBuffer.allocate(1)
    gl.glGetShaderPrecisionFormat(shaderType, precisionType, range, precision)
    new GLShaderPrecisionFormat(range.get(0), range.get(1), precision.get(0))
  }

  @inline
  def getShaderInfoLog(shader: GLShader): String = {
    val lengths = Array[Int](0)
    gl.glGetShaderiv(shader.id, GL2ES2.GL_INFO_LOG_LENGTH, lengths, 0)
    if(lengths(0) == 0)
      return ""
    val buf = new Array[Byte](lengths(0))
    gl.glGetShaderInfoLog(shader.id, lengths(0), lengths, 0, buf, 0)
    new String(buf, "UTF-8")
  }

  @inline
  def getShaderSource(shader: GLShader): String = {
    val lengths = Array[Int](0)
    gl.glGetProgramiv(shader.id, GL2ES2.GL_SHADER_SOURCE_LENGTH, lengths, 0)
    if(lengths(0) == 0)
      return ""
    val buf = new Array[Byte](lengths(0))
    gl.glGetShaderSource(shader.id, lengths(0), lengths, 0, buf, 0)
    new String(buf, "UTF-8")
  }

  @inline
  def getTexParameter(target: Int, pname: Int): Int = {
    val out = IntBuffer.allocate(1)
    gl.glGetTexParameteriv(target, pname, out)
    out.get(0)
  }

//  def getUniform(program: GLProgram, location: GLUniformLocation): Any

  @inline
  def getUniformLocation(program: GLProgram, name: String): GLUniformLocation =
    new JOGAMPUniformLocation(gl.glGetUniformLocation(program.id, name))

  def getVertexAttrib(index: Int, pname: Int): Any = {
    val out = IntBuffer.allocate(1)
    gl.glGetVertexAttribiv(index, pname, out)
    pname match {
      case NGL.VERTEX_ATTRIB_ARRAY_BUFFER_BINDING =>
        glBuffers.getOrElse(out.get(0), null)
      case NGL.VERTEX_ATTRIB_ARRAY_ENABLED =>
        out.get(0) != 0
      case NGL.VERTEX_ATTRIB_ARRAY_SIZE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_STRIDE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_TYPE =>
        out.get(0)
      case NGL.VERTEX_ATTRIB_ARRAY_NORMALIZED =>
        out.get(0) != 0
      case NGL.CURRENT_VERTEX_ATTRIB =>
        out
    }
  }

  @inline
  def hint(target: Int, mode: Int): Unit =
    gl.glHint(target, mode)

  @inline
  def isBuffer(buffer: GLBuffer): Boolean =
    buffer != null && gl.glIsBuffer(buffer.id)

  @inline
  def isEnabled(cap: Int): Boolean =
    gl.glIsEnabled(cap)

  @inline
  def isFramebuffer(framebuffer: GLFramebuffer): Boolean =
    framebuffer != null && gl.glIsFramebuffer(framebuffer.id)

  @inline
  def isProgram(program: GLProgram): Boolean =
    program != null && gl.glIsProgram(program.id)

  @inline
  def isRenderbuffer(renderbuffer: GLRenderbuffer): Boolean =
    renderbuffer != null && gl.glIsRenderbuffer(renderbuffer.id)

  @inline
  def isShader(shader: GLShader): Boolean =
    shader != null && gl.glIsShader(shader.id)

  @inline
  def isTexture(texture: GLTexture): Boolean =
    texture != null && gl.glIsTexture(texture.id)

  @inline
  def lineWidth(width: Float): Unit =
    gl.glLineWidth(width)

  @inline
  def linkProgram(program: GLProgram): Unit =
    gl.glLinkProgram(program.id)

  @inline
  def pixelStorei(pname: Int, param: Int): Unit =
    gl.glPixelStorei(pname, param)

  @inline
  def polygonOffset(factor: Float, units: Float): Unit =
    gl.glPolygonOffset(factor, units)

  @inline
  def readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.glReadPixels(x, y, width, height, format, `type`, pixels)

  @inline
  def renderbufferStorage(target: Int, internalFormat: Int, width: Int, height: Int): Unit =
    gl.glRenderbufferStorage(target, internalFormat, width, height)

  @inline
  def sampleCoverage(value: Int, invert: Boolean): Unit =
    gl.glSampleCoverage(value, invert)

  @inline
  def scissor(x: Int, y: Int, width: Int, height: Int): Unit =
    gl.glScissor(x, y, width, height)

  @inline
  def shaderSource(shader: GLShader, source: String): Unit =
    gl.glShaderSource(shader.id, 1, Array(source), null)

  @inline
  def stencilFunc(func: Int, ref: Int, mask: Int): Unit =
    gl.glStencilFunc(func, ref, mask)

  @inline
  def stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int): Unit =
    gl.glStencilFuncSeparate(face, func, ref, mask)

  @inline
  def stencilMask(mask: Int): Unit =
    gl.glStencilMask(mask)

  @inline
  def stencilMaskSeparate(face: Int, mask: Int): Unit =
    gl.glStencilMaskSeparate(face, mask)

  @inline
  def stencilOp(fail: Int, zFail: Int, zPass: Int): Unit =
    gl.glStencilOp(fail, zFail, zPass)

  @inline
  def stencilOpSeparate(face: Int, fail: Int, zFail: Int, zPass: Int): Unit =
    gl.glStencilOpSeparate(face, fail, zFail, zPass)

  @inline
  def texImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.glTexImage2D(target, level, internalFormat, width, height, border, format, `type`, pixels)

  @inline
  def texImage2D(target: Int, level: Int, internalFormat: Int, format: Int, `type`: Int, image: Image): Unit =
    image match {
      case pixels: ImageImpl =>
        val outBytes = new Array[Byte](pixels.width*pixels.height*4)
        pixels.peer.getRaster.getDataElements(0, 0, pixels.width, pixels.height, outBytes)
        val buffer = ByteBuffer.wrap(outBytes)
        gl.glTexImage2D(target, level, internalFormat, pixels.width, pixels.height, 0, format, `type`, buffer)
    }

//  def texImage2D(target: Int, level: Int, internalFormat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: Image): Unit

  @inline
  def texParameterf(target: Int, pname: Int, param: Float): Unit =
    gl.glTexParameterf(target, pname, param)

  @inline
  def texParameteri(target: Int, pname: Int, param: Int): Unit =
    gl.glTexParameteri(target, pname, param)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ByteBuffer): Unit =
    gl.glTexSubImage2D(target, level, xOffset, yOffset, width, height, format, `type`, pixels)

  @inline
  def texSubImage2D(target: Int, level: Int, xOffset: Int, yOffset: Int, format: Int, `type`: Int, image: Image): Unit =
    image match {
      case pixels: ImageImpl =>
        val outBytes = new Array[Byte](pixels.width*pixels.height*4)
        pixels.peer.getRaster.getDataElements(0, 0, pixels.width, pixels.height, outBytes)
        val buffer = ByteBuffer.wrap(outBytes)
        gl.glTexSubImage2D(target, level, xOffset, yOffset, pixels.width, pixels.height, format, `type`, buffer)
    }
  @inline
  def uniform1f(location: GLUniformLocation, x: Float): Unit =
    gl.glUniform1f(location.id, x)

  @inline
  def uniform1fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.glUniform1fv(location.id, 1, v)

  @inline
  def uniform1fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.glUniform1fv(location.id, 1, v, 0)

  @inline
  def uniform1i(location: GLUniformLocation, x: Int): Unit =
    gl.glUniform1i(location.id, x)

  @inline
  def uniform1iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.glUniform1iv(location.id, 1, v)

  @inline
  def uniform1iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.glUniform1iv(location.id, 1, v, 0)

  @inline
  def uniform2f(location: GLUniformLocation, x: Float, y: Float): Unit =
    gl.glUniform2f(location.id, x, y)

  @inline
  def uniform2fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.glUniform2fv(location.id, 1, v)

  @inline
  def uniform2fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.glUniform2fv(location.id, 1, v, 0)

  @inline
  def uniform2i(location: GLUniformLocation, x: Int, y: Int): Unit =
    gl.glUniform2i(location.id, x, y)

  @inline
  def uniform2iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.glUniform2iv(location.id, 1, v)

  @inline
  def uniform2iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.glUniform2iv(location.id, 1, v, 0)

  @inline
  def uniform3f(location: GLUniformLocation, x: Float, y: Float, z: Float): Unit =
    gl.glUniform3f(location.id, x, y, z)

  @inline
  def uniform3fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.glUniform3fv(location.id, 1, v)

  @inline
  def uniform3fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.glUniform3fv(location.id, 1, v, 0)

  @inline
  def uniform3i(location: GLUniformLocation, x: Int, y: Int, z: Int): Unit =
    gl.glUniform3i(location.id, x, y, z)

  @inline
  def uniform3iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.glUniform3iv(location.id, 1, v)

  @inline
  def uniform3iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.glUniform3iv(location.id, 1, v, 0)

  @inline
  def uniform4f(location: GLUniformLocation, x: Float, y: Float, z: Float, w: Float): Unit =
    gl.glUniform4f(location.id, x, y, z, w)

  @inline
  def uniform4fv(location: GLUniformLocation, v: FloatBuffer): Unit =
    gl.glUniform4fv(location.id, 1, v)

  @inline
  def uniform4fv(location: GLUniformLocation, v: Array[Float]): Unit =
    gl.glUniform4fv(location.id, 1, v, 0)

  @inline
  def uniform4i(location: GLUniformLocation, x: Int, y: Int, z: Int, w: Int): Unit =
    gl.glUniform4i(location.id, x, y, z, w)

  @inline
  def uniform4iv(location: GLUniformLocation, v: IntBuffer): Unit =
    gl.glUniform4iv(location.id, 1, v)

  @inline
  def uniform4iv(location: GLUniformLocation, v: Array[Int]): Unit =
    gl.glUniform4iv(location.id, 1, v, 0)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.glUniformMatrix2fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix2fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.glUniformMatrix2fv(location.id, 1, transpose, value, 0)

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.glUniformMatrix3fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix3fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.glUniformMatrix3fv(location.id, 1, transpose, value, 0)

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: FloatBuffer): Unit =
    gl.glUniformMatrix4fv(location.id, 1, transpose, value)

  @inline
  def uniformMatrix4fv(location: GLUniformLocation, transpose: Boolean, value: Array[Float]): Unit =
    gl.glUniformMatrix4fv(location.id, 1, transpose, value, 0)

  @inline
  def useProgram(program: GLProgram): Unit =
    gl.glUseProgram(program.id)

  @inline
  def validateProgram(program: GLProgram): Unit =
    gl.glValidateProgram(program.id)

  @inline
  def vertexAttrib1f(indx: Int, x: Float): Unit =
    gl.glVertexAttrib1f(indx, x)

  @inline
  def vertexAttrib1fv(indx: Int, values: FloatBuffer): Unit =
    gl.glVertexAttrib1f(indx, values.get(0))

  @inline
  def vertexAttrib1fv(indx: Int, values: Array[Float]): Unit =
    gl.glVertexAttrib1f(indx, values(0))

  @inline
  def vertexAttrib2f(indx: Int, x: Float, y: Float): Unit =
    gl.glVertexAttrib2f(indx, x, y)

  @inline
  def vertexAttrib2fv(indx: Int, values: FloatBuffer): Unit =
    gl.glVertexAttrib2f(indx, values.get(0), values.get(1))

  @inline
  def vertexAttrib2fv(indx: Int, values: Array[Float]): Unit =
    gl.glVertexAttrib2f(indx, values(0), values(1))

  @inline
  def vertexAttrib3f(indx: Int, x: Float, y: Float, z: Float): Unit =
    gl.glVertexAttrib3f(indx, x, y, z)

  @inline
  def vertexAttrib3fv(indx: Int, values: FloatBuffer): Unit =
    gl.glVertexAttrib3f(indx, values.get(0), values.get(1), values.get(2))

  @inline
  def vertexAttrib3fv(indx: Int, values: Array[Float]): Unit =
    gl.glVertexAttrib3f(indx, values(0), values(1), values(2))

  @inline
  def vertexAttrib4f(indx: Int, x: Float, y: Float, z: Float, w: Float): Unit =
    gl.glVertexAttrib4f(indx, x, y, z, w)

  @inline
  def vertexAttrib4fv(indx: Int, values: FloatBuffer): Unit =
    gl.glVertexAttrib4f(indx, values.get(0), values.get(1), values.get(2), values.get(3))

  @inline
  def vertexAttrib4fv(indx: Int, values: Array[Float]): Unit =
    gl.glVertexAttrib4f(indx, values(0), values(1), values(2), values(3))

  @inline
  def vertexAttribPointer(indx: Int, size: Int, `type`: Int, normalized: Boolean, stride: Int, offset: Int): Unit =
    gl.glVertexAttribPointer(indx, size, `type`, normalized, stride, offset)

  @inline
  def viewport(x: Int, y: Int, width: Int, height: Int): Unit =
    gl.glViewport(x, y, width, height)
}
