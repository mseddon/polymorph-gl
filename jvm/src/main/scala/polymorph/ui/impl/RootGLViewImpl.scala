package polymorph.ui.impl

import java.awt._
import java.awt.event.{WindowEvent, WindowAdapter}

import com.jogamp.opengl.util.FPSAnimator
import com.jogamp.opengl.{GLRunnable, GLEventListener, GL2ES2, GLAutoDrawable}
import com.jogamp.opengl.awt.GLCanvas
import polymorph.event.impl.{MouseEventReceiverProxy, KeyEventReceiverProxy}
import polymorph.gl.GL
import polymorph.gl.impl.JOGAMP
import polymorph.ui.{RootGLViewPeer, RootGLView}
import scryetek.vecmath.Vec2

class RootGLViewImpl(view: RootGLView, _title: String, withId: String) extends RootGLViewPeer(view) {
  System.setProperty("sun.awt.noerasebackground", "true")

  var _peer = new Frame(_title + "patch")

  def width: Float =
    canvas.getWidth

  def visible_=(visible: Boolean): Unit =
    _peer.setVisible(visible)

  def visible: Boolean =
    _peer.isVisible

  def title: String =
    _peer.getTitle

  def height: Float =
    canvas.getHeight

  def title_=(title: String): Unit =
    _peer.setTitle(title)

  def globalPosition: Vec2 =
    Vec2(0,0)

  _peer.addWindowListener(new WindowAdapter {
    override def windowClosing(windowEvent: WindowEvent): Unit = {
      System.exit(0)
    }
  })
  _peer.setSize(800, 600)

  class Canvas extends GLCanvas with KeyEventReceiverProxy with MouseEventReceiverProxy {
    val receiver = view
    enableEvents(AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.MOUSE_WHEEL_EVENT_MASK)
  }

  var drawable: GLAutoDrawable = null

  var gl: JOGAMP = null

  var canvas = new Canvas
  val animator = new FPSAnimator(canvas, 60);

  canvas.addGLEventListener(new GLEventListener {
    override def init(glAutoDrawable: GLAutoDrawable): Unit = {
      drawable = glAutoDrawable
      gl = new JOGAMP(glAutoDrawable.getContext.getGL.asInstanceOf[GL2ES2])
      view.init(gl)
    }

    override def display(glAutoDrawable: GLAutoDrawable): Unit = {
      drawable = glAutoDrawable
      gl.gl = glAutoDrawable.getGL.asInstanceOf[GL2ES2]
      view.paint(gl)
    }

    override def reshape(glAutoDrawable: GLAutoDrawable, i: Int, i1: Int, i2: Int, i3: Int): Unit = {
    }

    override def dispose(glAutoDrawable: GLAutoDrawable): Unit = {
    }
  })

  _peer.add(canvas)

  def withGL(body: GL => Unit): Unit = {
    canvas.invoke(true, new GLRunnable {
      override def run(glAutoDrawable: GLAutoDrawable): Boolean = {
        drawable = glAutoDrawable
        gl.gl = glAutoDrawable.getGL.asInstanceOf[GL2ES2]
        body(gl)
        false
      }
    })
  }

  _peer.add(canvas)
  canvas.setPreferredSize(new Dimension(640, 480))
  _peer.pack()

  override def repaint(): Unit =
    canvas.repaint()

  def focus(): Unit =
    canvas.requestFocus()

  def blur(): Unit =
    KeyboardFocusManager.getCurrentKeyboardFocusManager.clearGlobalFocusOwner()

  private var _animate = false

  /** Returns if the GLApp is currently animating */
  def animate = _animate
  /** Make the GLApp animate continuously or not. */
  def animate_=(b: Boolean): Unit = {
    if(_animate != b)
      _animate = b
    if(_animate)
      animator.start()
    else
      animator.stop()
  }
}

object RootGLViewImpl {
  def apply(view: RootGLView, title: String, withId: String): RootGLViewPeer =
    new RootGLViewImpl(view, title, withId)
}
