name := "Scryetek Polymorph GL"

publish := {}
publishLocal := {}

lazy val polymorphGl = polymorphLibrary.in(file(".")).settings(
  name := "polymorph-gl",
  organization := "com.scryetek",
  version := "0.1-SNAPSHOT",
  polymorphLinkerObject := Some("polymorph.gl.impl.Linker"),

  libraryDependencies += polylib("com.scryetek" %%% "polymorph-core" % "0.1-SNAPSHOT"),
  scalaVersion := "2.11.7",
  resolvers += "Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local",
  credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
  publishTo := Some("Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local")
).jvmSettings(
    libraryDependencies ++= Seq(
      "org.jogamp.jogl" % "jogl" % "2.3.1",
      "org.jogamp.jogl" % "jogl-all-main" % "2.3.1",
      "org.jogamp.gluegen" % "gluegen-rt-main" % "2.3.1"
    )
)

lazy val polymorphGlJS = polymorphGl.js
lazy val polymorphGlJVM = polymorphGl.jvm
lazy val polymorphGlIOS = polymorphGl.ios
lazy val polymorphGlAndroid = polymorphGl.android